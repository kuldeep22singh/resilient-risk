//
//  MyReportsTableViewCell.swift
//  ResilientRisk
//
//  Created by apple on 14/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class MyReportsTableViewCell: UITableViewCell {

    //MARK::- Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNumberOfQuestions: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if bgView != nil {
            bgView.addShadows(color: UIColor.lightGray)
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var myReportModel : MyReportModel? {
        
        didSet {
            self.lblName.text = myReportModel?.categoryName ?? ""
            
            var strImage = myReportModel?.image ?? ""
            
            strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
            
            self.imgView.sd_setImage(with: URL.init(string: strImage), placeholderImage:  UIImage.init(named: "imgProfilePlace"), options: [.refreshCached], progress: nil, completed: nil)
        }
        
    }
    
    
    var reportDateModel : MyReportMyreports? {
        
        didSet {
            self.lblName.text = reportDateModel?.categoryName ?? ""
            
              let date = Date.init(milliseconds: reportDateModel?.reportTime ?? 0)
            
            self.lblDate.text = date.convertToFormat("dd MMM yyyy hh:mm a")
            
            self.lblNumberOfQuestions.text = "Number of Questions \(self.reportDateModel?.totalQuestions ?? 0)"
        }
        
    }

    
}

