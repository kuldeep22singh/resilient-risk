//
//  CategoryInfoTableViewCell.swift
//  ResilientRisk
//
//  Created by apple on 18/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CategoryInfoTableViewCell: UITableViewCell {
    //MARK:- Outlets
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblNumberOfQuestion: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblInformation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if bgView != nil {
            bgView.addShadows(color: UIColor.lightGray)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var homeData : HomeDataModel? {
        
        didSet {
            
            if self.lblName != nil {
                self.lblName.text = homeData?.name ?? ""
            }
            if  self.imgView != nil {
                
                var strImage = homeData?.image ?? ""
                
                strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
                
                
                self.imgView.sd_setImage(with: URL.init(string: strImage), placeholderImage:  UIImage.init(named: "imgProfilePlace"), options: [.refreshCached], progress: nil, completed: nil)
            }
            if   self.lblDescription != nil {
                self.lblDescription.text = homeData?.descriptionValue ?? ""
            }
            
            if    self.lblInformation != nil {
                self.lblInformation.text =  "Info About \(homeData?.name ?? "") Quiz"
            }
            
            if   self.lblNumberOfQuestion != nil {
                self.lblNumberOfQuestion.text = "\(homeData?.totalQuestions ?? 0) Questions in this Quiz."
            }
            
        }
        
    }
    
    
}
