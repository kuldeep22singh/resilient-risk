//
//  QuestionsTableViewCell.swift
//  ResilientRisk
//
//  Created by apple on 18/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class QuestionsTableViewCell:UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var lblQuestions: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNumberOfQuestions: UILabel!
    @IBOutlet weak var lblQuestionNumber: UILabel!
    @IBOutlet var backArrayView: [UIView]!
       
       @IBOutlet var nextArrayView: [UIView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
