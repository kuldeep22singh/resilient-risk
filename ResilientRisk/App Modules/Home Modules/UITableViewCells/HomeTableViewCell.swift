//
//  HomeTableViewCell.swift
//  ResilientRisk
//
//  Created by apple on 14/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    //MARK::- Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblResume: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if bgView != nil {
            bgView.addShadows(color: UIColor.lightGray)
        }
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var homeData : HomeDataModel? {
        
        didSet {
            
            let localdict : [String : Any] =  JSON(standard.value(forKey: "User\(UserProfile.shared.profile.internalIdentifier ?? 0)Category\(homeData?.internalIdentifier ?? 0)") ?? "").dictionaryObject ?? [String : Any]()
            
            
            let isLocalResume =  JSON(localdict)["category_id"].int == homeData?.internalIdentifier
            
            self.lblName.text = homeData?.name ?? ""
            
            var strImage = homeData?.image ?? ""
            
            strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
            
            self.imgView.sd_setImage(with: URL.init(string: strImage), placeholderImage:  UIImage.init(named: "imgProfilePlace"), options: [.refreshCached], progress: nil, completed: nil)
            
            self.lblResume.isHidden = !isLocalResume
            
            self.lblResume.text = isLocalResume == false ? "" : "  Resume Assessment  "
            
        }
        
    }
    
}
