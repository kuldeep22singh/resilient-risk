//
//  LeftMenuTableViewCell.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
