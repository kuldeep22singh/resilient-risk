//
//  ReportDetailsViewModel.swift
//  ResilientRisk
//
//  Created by apple on 27/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ReportDetailsViewModel: NSObject {
    
    
    var reportId = Int()
    
    var reportDetailsModel : ReportDetailModel?
    
    var currentIndex = 0
    
    var isFromMyReport = Bool()
    
    var homeData : HomeDataModel?
    
    
    func getReportDetails(onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.getReport.rawValue
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["report_id":self.reportId], isShowLoader: true, success: { (json) in
            print(json)
            
            
            self.reportDetailsModel = ReportDetailModel.init(json: json["body"])
            
            for i in 0..<(self.reportDetailsModel?.questions?.count ?? 0){
                
                if let index =   self.reportDetailsModel?.questions?[i].options?.firstIndex(where: {$0.answerId == self.reportDetailsModel?.questions?[i].myanswer }) {
                    
                    var option = self.reportDetailsModel?.questions?[i].options?[index].dictionaryRepresentation() ?? [String : Any]()
                    
                    option.updateValue("Description:- ", forKey: "option_index")
                    option.updateValue(-1, forKey: "answer_id")
                    option.updateValue(self.reportDetailsModel?.questions?[i].options?[index].descriptionValue ?? 0, forKey: "options")
                    
                    let data = ReportDetailOptions.init(json: JSON(option))
                    
                    self.reportDetailsModel?.questions?[i].options?.insert(data, at: index + 1)
                    
                    
                }
             
                    let options =      self.reportDetailsModel?.questions?[i].options?.filter({($0.answerId == self.reportDetailsModel?.questions?[i].myanswer ) || ($0.answerId == -1)})
                    
                    self.reportDetailsModel?.questions?[i].options = options
                
            }
            
            
            
            
            
            
            
            DispatchQueue.main.async {
                
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            rootVC?.showSwiftyAlert("", error, false)
        }
        
    }
    
    func restartQuestions( onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.restartQuestion.rawValue
        
        let param : Parameters =  ["category_id":reportDetailsModel?.categoryId ?? 0 , "report_id" : self.reportId]
        
        print(param)
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: param, isShowLoader: true, success: { (json) in
            print(json)
            
            self.homeData = HomeDataModel.init(json: json["body"])
            
            DispatchQueue.main.async {
                
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            rootVC?.showSwiftyAlert("", error, false)
        }
        
    }
    
    func saveReport( onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.savereport.rawValue
        
        let param : Parameters =  [ "report_id" : self.reportId]
        
        print(param)
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: param, isShowLoader: true, success: { (json) in
            print(json)
            
            DispatchQueue.main.async {
                
                rootVC?.showSwiftyAlert("", json["message"].stringValue, true)
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            rootVC?.showSwiftyAlert("", error, false)
        }
        
    }
    
    
}
