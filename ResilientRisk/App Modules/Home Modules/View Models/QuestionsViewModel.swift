//
//  QuestionsViewModel.swift
//  ResilientRisk
//
//  Created by apple on 24/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class QuestionsViewModel: NSObject {
    
    var categoryId = Int()
    
    var questionModel : QuestionModel?
    
    var localQuestionModel : QuestionModel?
    
    var noAnswerQuestions : QuestionModel?
    
    var currentIndex = 0
    
    var reportId = Int()
    
    func getQuestions(category_id : Int , onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.getQuestions.rawValue
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["category_id":category_id], isShowLoader: true, success: { (json) in
            print(json)
            
            self.questionModel = QuestionModel.init(json : json["body"])
            
      
            
            for i in 0..<(self.questionModel?.questions?.count ?? 0){
                
                self.questionModel?.questions?[i].answerId = 0
            }
            
            
            self.noAnswerQuestions = QuestionModel.init(json : json["body"])
            
            let localdict : [String : Any] =  JSON(standard.value(forKey: "User\(UserProfile.shared.profile.internalIdentifier ?? 0)Category\(self.categoryId )") ?? "").dictionaryObject ?? [String : Any]()
            
            self.localQuestionModel =  QuestionModel.init(json: JSON(localdict))
            
           let localQuestion = self.localQuestionModel?.questions ?? []
            
            for question in localQuestion {
                if let index = self.questionModel?.questions?.firstIndex(where: {$0.internalIdentifier == question.internalIdentifier}){
                    self.questionModel?.questions?[index] = question
                }
            
            }
            
            self.noAnswerQuestions?.questions =     self.questionModel?.questions?.filter({$0.answerId == 0})
            
            DispatchQueue.main.async {
                
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            rootVC?.showSwiftyAlert("", error, false)
        }
        
    }
    

    func saveExitQuestions( viewModel : QuestionsViewModel, onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.saveExitQuestions.rawValue
        
        var options = [[String : Any]]()
        
        for i in 0..<(viewModel.questionModel?.questions?.count  ?? 0){
            
            let status = viewModel.questionModel?.questions?[i].options?.contains(where: {$0.answerId == viewModel.questionModel?.questions?[i].answerId }) ?? false
            
            let dict : [ String : Any ] = ["question_id":viewModel.questionModel?.questions?[i].internalIdentifier ?? 0,
                                           "status":status == true ? 1 : 0,
                                           "option":status == true ? "\((viewModel.questionModel?.questions?[i].answerId ?? 0))" : ""
            ]
            options.append(dict)
        }
        
        
        let param : Parameters = ["category_id":viewModel.categoryId , "options" :options.toJSONString() ]
        print(param)
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: param, isShowLoader: true, success: { (json) in
            print(json)
            
            self.reportId =  json["body"]["report_id"].intValue
            
            DispatchQueue.main.async {
                rootVC?.showSwiftyAlert("", json["message"].stringValue, true)
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            rootVC?.showSwiftyAlert("", error, false)
        }
        
    }
    
    
}
