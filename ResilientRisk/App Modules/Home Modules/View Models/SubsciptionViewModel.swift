//
//  SubsciptionViewModel.swift
//  ResilientRisk
//
//  Created by apple on 24/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SubsciptionViewModel: NSObject {

    func purchaseSubscription(onSuccess:@escaping (successResponse)){
           
           let url = Apis.baseUrl.rawValue + Apis.purhcaseSubscription.rawValue
           
           ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["":""], isShowLoader: true, success: { (json) in
               print(json)
               
            var dict = UserProfile.shared.profile.dictionaryRepresentation()
            dict.updateValue(json["body"]["isPurchasePlan"].intValue, forKey: "isPurchasePlan")
            
            UserProfile.shared.profile = UserDataModel.init(json: JSON(dict))
            
               DispatchQueue.main.async {
                   
                   onSuccess()
                   
               }
               
           }) { (error) in
               print(error)
             rootVC?.showSwiftyAlert("", error, false)
           }
        
       }
}
