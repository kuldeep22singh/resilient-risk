//
//  MyReportViewModel.swift
//  ResilientRisk
//
//  Created by apple on 27/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class MyReportViewModel: NSObject {
    
    
    var myReportsArray = [MyReportModel]()
    
     var myReport : MyReportModel?
    
    var categoryId = Int()
    
    func getMyReports( onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.getMyReports.rawValue
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["":""], isShowLoader: true, success: { (json) in
            print(json)
            
            if let array = json["body"].array {
                
                self.myReportsArray = array.map({MyReportModel.init(json: $0)})
                
            }
              self.myReportsArray = self.myReportsArray.sorted(by: {($0.reportTime ?? 0) > ($1.reportTime ?? 0)})
            DispatchQueue.main.async {
               
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            DispatchQueue.main.async {
               
                onSuccess()
                
            }
            rootVC?.showSwiftyAlert("", error, false)
        }
        
    }

    func getReportsDates( onSuccess:@escaping (successResponse)){
           
           let url = Apis.baseUrl.rawValue + Apis.reportDates.rawValue
           
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["category_id":self.categoryId], isShowLoader: true, success: { (json) in
               print(json)
                   
                self.myReport = MyReportModel.init(json: json["body"])
                 
               DispatchQueue.main.async {
                  
                   onSuccess()
                   
               }
               
           }) { (error) in
               print(error)
               DispatchQueue.main.async {
                  
                   onSuccess()
                   
               }
               rootVC?.showSwiftyAlert("", error, false)
           }
           
       }
    
}
