//
//  HomeDataViewModel.swift
//  ResilientRisk
//
//  Created by apple on 24/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HomeDataViewModel: NSObject {

    
    var homeData : HomeDataModel?
    
    var homeDataArray = [HomeDataModel]()
    
    func homeData(onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.gethomedata.rawValue
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["":""], isShowLoader: true, success: { (json) in
            print(json)
        
            
            if let array = json["body"].array {
                self.homeDataArray = array.map({HomeDataModel.init(json: $0)})
            }
            
            DispatchQueue.main.async {
                
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
          rootVC?.showSwiftyAlert("", error, false)
        }
     
    }
    
}
