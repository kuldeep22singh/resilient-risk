//
//  SubscriptionViewController.swift
//  ResilientRisk
//
//  Created by apple on 12/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SwiftyStoreKit
class SubscriptionViewController: UIViewController {

    //MARK:- Variables
     let inAppId =  "com.risk.NC99Plan2"
    lazy var viewModel = SubsciptionViewModel()
    
    //MARK:- Outlets
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.addShadows(color: UIColor.lightGray)
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnBuyAction(_ sender: UIButton) {
       
     inAppPurchaseDataFetch()
    
    }
    
    //MARK:- IN APP PURCHASE DATA
    func inAppPurchaseDataFetch(){
     
        
        rootVC?.showLoader()
        
        SwiftyStoreKit.retrieveProductsInfo([inAppId]) { result in
            
            print(result)
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice ?? "0"
                print("Product: \(product.localizedDescription), price: \(priceString)")
                
                
                
                SwiftyStoreKit.purchaseProduct(product, quantity: 1, atomically: true) { result in
                   rootVC?.hideLoader()
                    switch result {
                    case .success(let purchase):
                        print("Purchase Success: \(purchase.productId)")

                        SwiftyStoreKit.finishTransaction(purchase.transaction)

                        DispatchQueue.main.async {
                            self.purchaseSubcription()
                        }
                        

                    case .error(let error):

                        print("Error")

                        switch error.code {
                        case .unknown: print("Unknown error. Please contact support")
                        case .clientInvalid: print("Not allowed to make the payment")
                        case .paymentCancelled: break
                        case .paymentInvalid: print("The purchase identifier was invalid")
                        case .paymentNotAllowed: print("The device is not allowed to make the payment")
                        case .storeProductNotAvailable: print("The product is not available in the current storefront")
                        case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                        case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                        case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                        case .privacyAcknowledgementRequired:
                            print("Privacy Acknowledgement Required")
                        case .unauthorizedRequestData:
                               print("Unauthorized RequestData")
                        case .invalidOfferIdentifier:
                                print("Invalid Offer Identifier")
                        case .invalidSignature:
                               print("invalid Signature")
                        case .missingOfferParams:
                             print("Missing Offer Params")
                        case .invalidOfferPrice:
                            print("invalid Offer Price")
                        @unknown default:
                              print("Other Error")
                        }
                    }
                }
            }
            else if result.invalidProductIDs.first != nil {
                rootVC?.hideLoader()
               rootVC?.showSwiftyAlert("", "Could not retrieve product info", false)
        
            }
            else {
                rootVC?.hideLoader()
                rootVC?.showSwiftyAlert("", result.error?.localizedDescription ?? "", false)
        
            }
        }
        
    }
    
    
    //MARK:- Purchase Subscription
    func purchaseSubcription(){
        
        viewModel.purchaseSubscription {
             
            DispatchQueue.main.async {
                   self.navigationController?.popViewController(animated: true)
//                let vc = PaymentSuccessView.init(frame: rootVC?.view.frame ?? CGRect() , strTitle : "Your have sucessfully purchase $9.99 Package.") {
//                    self.navigationController?.popViewController(animated: true)
//                       }
//
//                        rootVC?.view.addSubview(vc)
            }
            
        }
        
    }
    
}
