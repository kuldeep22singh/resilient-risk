//
//  QuestionsViewController.swift
//  ResilientRisk
//
//  Created by apple on 18/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController {
    
    
    //MARK:- Variables
    
    lazy var viewModel = QuestionsViewModel()
    
   
    
    //MARK:- Outlets
    
    @IBOutlet var backArrayView: [UIView]!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblQuestionNumber: UILabel!
    @IBOutlet weak var btnSaveExit: UIButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnSaveExit.setAttributedTitle(self.setAttributeString(), for: .normal)
        
        self.getQuestions()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnSkipAction(_ sender: UIButton) {
        
       
        
        guard viewModel.currentIndex < (viewModel.noAnswerQuestions?.questions?.count ?? 0) - 1 else{
            
            guard  UserProfile.shared.profile.isPurchasePlan == 1 else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController

             self.navigationController?.pushViewController(vc, animated: true)
                return

            }
            
        
            self.saveAndExitQuestions()
            return
        }
        viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].answerId = 0
        viewModel.currentIndex = viewModel.currentIndex + 1
        
        self.tableView.reloadData()
        
    }
    
    @IBAction func btnBackArrowAction(_ sender: UIButton) {
        guard viewModel.currentIndex > 0 else{
            
            return
        }
        
        viewModel.currentIndex = viewModel.currentIndex - 1
        
        lblQuestionNumber.text = "Question \(viewModel.currentIndex + 1)"
        
        self.tableView.reloadData()
        
    }
    
    @IBAction func btnBottomNextAction(_ sender: UIButton) {
        
        guard viewModel.currentIndex < (viewModel.noAnswerQuestions?.questions?.count ?? 0) - 1 else{
            guard  UserProfile.shared.profile.isPurchasePlan == 1 else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController

                self.navigationController?.pushViewController(vc, animated: true)
                return

            }

            self.saveAndExitQuestions()
        
            return
        }
        
        viewModel.currentIndex = viewModel.currentIndex + 1
        
        self.tableView.reloadData()
    }
    
    
    @IBAction func btnRestartAction(_ sender: UIButton) {
    
        let vc = AlertView.init(frame: rootVC?.view.frame ?? CGRect() , strTitle: "Restart", strDescription: "Are you sure that you\nwant to restart?") { (index) in
            
            if index == 1{
                self.restartQuestion()
            }
        
        }
        
        rootVC?.view.addSubview(vc)
        
    }
    @IBAction func btnSaveAndExitAction(_ sender: UIButton) {
        
        let vc = AlertView.init(frame: rootVC?.view.frame ?? CGRect() , strTitle: "Save", strDescription: "Are you sure you want\nto save and exit?") { (index) in
            
            print(index)
            
            if index == 1{
                
    
                self.localSaveAndExit()
                
            }
        }
        
        rootVC?.view.addSubview(vc)
    }
    //MARK:- SetAttributeString
    func setAttributeString() -> NSMutableAttributedString{
        
        let main_string = "Save & Exit"
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        
        
        attribute.addAttributes([ NSAttributedString.Key.font : UIFont(name: "Raleway", size: 17)! ,
                                  
                                  NSAttributedString.Key.foregroundColor: UIColor.red , .underlineStyle: NSUnderlineStyle.single.rawValue], range: NSRange.init(location: 0, length: main_string.count))
        
        
        return attribute
    }
    
    func setUIData(){
        
        backArrayView.forEach { (view) in
            
            view.isHidden = viewModel.currentIndex == 0
            
        }
        
        btnSaveExit.isHidden = viewModel.currentIndex == ((viewModel.noAnswerQuestions?.questions?.count ?? 0) - 1)
        
        if  viewModel.noAnswerQuestions?.questions?.indices.contains(viewModel.currentIndex) == true {
            let questionId  = viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].internalIdentifier ?? 0
            
            let index = viewModel.questionModel?.questions?.firstIndex(where: {$0.internalIdentifier ==  questionId}) ?? 0
            
            lblQuestionNumber.text = "Question \(index + 1)"
            
            
            self.title = "\(viewModel.noAnswerQuestions?.categoryName ?? "") \(index + 1)/\(viewModel.questionModel?.questions?.count ?? 0)"
            
            self.btnNext.setTitle(viewModel.currentIndex < (viewModel.noAnswerQuestions?.questions?.count ?? 0) - 1 ? "Next" : "Finish", for: .normal)
            
        }
            
            
            
           
        
    }
    
    //MARK:- Get Questions
    func getQuestions(){
        
        viewModel.getQuestions(category_id: viewModel.categoryId) {
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    func restartQuestion(){
        
    standard.removeObject(forKey: "User\(UserProfile.shared.profile.internalIdentifier ?? 0)Category\(self.viewModel.categoryId)")
        
        self.viewModel.currentIndex = 0
      
         self.getQuestions()

    }
    
    
    func localSaveAndExit(){
        
        for i in 0..<(viewModel.noAnswerQuestions?.questions?.count  ?? 0){
            
            if let index =     viewModel.questionModel?.questions?.firstIndex(where: {$0.internalIdentifier == viewModel.noAnswerQuestions?.questions?[i].internalIdentifier}) {
                if let question = viewModel.noAnswerQuestions?.questions?[i] {
                    viewModel.questionModel?.questions?[index] =  question
                }
            }
            
        }
        
        let questionsDict : [String : Any] = self.viewModel.questionModel?.dictionaryRepresentation() ?? [String : Any]()
        
        print(JSON(questionsDict))
        
        standard.set(questionsDict, forKey: "User\(UserProfile.shared.profile.internalIdentifier ?? 0)Category\(self.viewModel.categoryId)")
        
        HelperClass.sharedInstance.gotoHome()
        rootVC?.showSwiftyAlert("","The feedback is submited", true)
    }
    
    
    func saveAndExitQuestions(){
        
       for i in 0..<(viewModel.noAnswerQuestions?.questions?.count  ?? 0){
           
           if let index =     viewModel.questionModel?.questions?.firstIndex(where: {$0.internalIdentifier == viewModel.noAnswerQuestions?.questions?[i].internalIdentifier}) {
               if let question = viewModel.noAnswerQuestions?.questions?[i] {
                            viewModel.questionModel?.questions?[index] =  question
                      }
                  }
       
       }
        
        viewModel.saveExitQuestions( viewModel: self.viewModel) {
            DispatchQueue.main.async {

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportDetailViewController") as! ReportDetailViewController
                vc.viewModel.reportId = self.viewModel.reportId
                vc.refreshQuestion = {
                    self.viewModel.currentIndex = 0
                    self.getQuestions()
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    
    
}
extension QuestionsViewController : UITableViewDataSource , UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch   viewModel.noAnswerQuestions?.questions?.indices.contains(viewModel.currentIndex)  {
        case true:
            self.btnNext.backgroundColor =   viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].answerId != 0 ? appColor :  UIColor.init(red: 255/255, green: 209/255, blue:140/255, alpha: 1.0)
            
            self.btnNext.isUserInteractionEnabled = viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].answerId != 0
            
            self.setUIData()
            return  section == 0 ? 1 :  (viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].options?.count ?? 0 )
            
        default:
            self.btnNext.backgroundColor =  UIColor.init(red: 255/255, green: 209/255, blue:140/255, alpha: 1.0)
            
            self.btnNext.isUserInteractionEnabled = false
            
            self.setUIData()
            
            return    0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionsTableViewCell", for: indexPath) as! QuestionsTableViewCell
        
        
        switch indexPath.section {
        case 0:
            cell.lblQuestions.font = UIFont(name: "Raleway-Medium", size: 17) ?? UIFont()
            cell.lblQuestions.text =  viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].question ?? ""
            cell.bgView.backgroundColor  = UIColor.white
            
            return cell
        default:
            
            cell.lblQuestions.font = UIFont(name: "Raleway", size: 17) ?? UIFont()
            
            let index =  viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].options?[indexPath.row].optionIndex ?? ""
            
            cell.lblQuestions.text =  "\(index)) " + (viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].options?[indexPath.row].options ?? "")
            
            let answerId = viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].options?[indexPath.row].answerId ?? 0
            
            cell.bgView.backgroundColor = viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].answerId == answerId  ?   appColor : UIColor.white
            
            return cell
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard indexPath.section == 1 else {
            
            return
        }
        
        let answerId = viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].options?[indexPath.row].answerId ?? 0
        
        
        let previousAnswerId = viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].answerId
        
        viewModel.noAnswerQuestions?.questions?[viewModel.currentIndex].answerId =     previousAnswerId ==  answerId ? 0 :  answerId
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

