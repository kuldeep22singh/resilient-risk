//
//  ReportDetailViewController.swift
//  ResilientRisk
//
//  Created by apple on 18/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import MessageUI

class ReportDetailViewController: UIViewController {
    
    //MARK:- Variables
    
    lazy var viewModel = ReportDetailsViewModel()
    
     var refreshQuestion : (()->() )?
    //MARK:- Outlets
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getReportDetails()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReAttemptAction(_ sender: UIButton) {
        
        
        let vc = AlertView.init(frame: rootVC?.view.frame ?? CGRect() , strTitle: "Re-Attempt", strDescription: "Are you sure that you\nwant to Re-Attempt?") { (index) in
            
            if index == 1{
                self.apiReAttemp()
            }
            
        }
        
        rootVC?.view.addSubview(vc)
        
    }
    
    @IBAction func btnSaveReportAction(_ sender: UIButton) {
        
        guard viewModel.isFromMyReport == false else {
            
            rootVC?.showSwiftyAlert("", "Report already saved", false)
            
            return
        }
        
        self.saveReport()
       
    }
    @IBAction func btnAirPrintAction(_ sender: UIButton) {
        
        airPrint()
    }
    
    @IBAction func btnShareViaEmailAction(_ sender: UIButton) {
        
        var str = self.viewModel.reportDetailsModel?.pdfFile ?? ""
        
        str = str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        

        if let url  = URL.init(string: str) {
            
            FileDownloader.loadFileAsync(url: url) { (data , url, error) in
                
                guard data != nil  else{
                    
                    return
                }
                
                DispatchQueue.main.async {
                    self.sendEmail(data: data)
                }
                
            }
            
        }
        
        
        
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        guard viewModel.currentIndex < (viewModel.reportDetailsModel?.questions?.count ?? 0) - 1 else{
            
            return
        }
        
        viewModel.currentIndex = viewModel.currentIndex + 1
        
        self.tableView.reloadData()
    }
    
    @IBAction func btnPreviousAction(_ sender: UIButton) {
        
        guard viewModel.currentIndex > 0 else{
            
            return
        }
        
        viewModel.currentIndex = viewModel.currentIndex - 1
        
        self.tableView.reloadData()
    }
     
    
    func airPrint(){
        
        var str = self.viewModel.reportDetailsModel?.pdfFile ?? ""
        
        str = str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        guard let url = URL(string:str) else{
            return
        }
        
        let printController = UIPrintInteractionController.shared
        let printInfo = UIPrintInfo(dictionary: [:])
        printInfo.outputType = UIPrintInfo.OutputType.general
        printInfo.orientation = UIPrintInfo.Orientation.portrait
        printInfo.jobName = "Report"
        printController.printInfo = printInfo
        
        printController.printingItem = NSData(contentsOf:url)
        
        printController.present(animated: true) { (controller, completed, error) in
            if(!completed && error != nil){
                
                print("Print failed - \(error?.localizedDescription ?? "")")
                
            }
            else if(completed) {
                
                print("Print succeeded")
            }
        }
        
    }
        
    
    
    //MARK:- Send Email
    
    func sendEmail(data:Data?){
        if( MFMailComposeViewController.canSendMail() ) {
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            mailComposer.setToRecipients([])
            mailComposer.setSubject("\(self.viewModel.reportDetailsModel?.categoryName ?? "") Report")
            mailComposer.setMessageBody("Report Details", isHTML: true)

            if let fileData = data {
                mailComposer.addAttachmentData(fileData, mimeType: "application/pdf", fileName: "\(self.viewModel.reportDetailsModel?.categoryName ?? "")).pdf")
           }


           self.present(mailComposer, animated: true, completion: nil)
                return
        }
        print("Email is not configured")

    }
    
    
    //MARK:- Re-Attemp Api
    func apiReAttemp(){
         
        viewModel.restartQuestions {
            standard.removeObject(forKey: "User\(UserProfile.shared.profile.internalIdentifier ?? 0)Category\(self.viewModel.reportDetailsModel?.categoryId ?? 0)")
            guard  self.viewModel.isFromMyReport == true  else {
                
                
                guard  let refreshQuestion = self.refreshQuestion else {
                    
                     self.navigationController?.popViewController(animated: true)
                    return
                }
                refreshQuestion()
                self.navigationController?.popViewController(animated: true)
                
                return
            }
            
            let homeVc =  self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
              let categoryVc = self.storyboard?.instantiateViewController(withIdentifier: "CategoryInformationViewController") as! CategoryInformationViewController
            
                categoryVc.viewModel.homeData =  self.viewModel.homeData
            
            
               self.navigationController?.viewControllers = [homeVc ,  categoryVc]
            
               NotificationCenter.default.post(name: Notification.Name("leftMenuReload"), object: ["index":0])
            
        
        }

    }
    
    //MARK:- Get Report Detail
    func getReportDetails(){
        
        viewModel.getReportDetails {
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    //MARK:- Save Report Details
    
    func saveReport(){
        
        viewModel.saveReport {
            DispatchQueue.main.async {
                
                        let reportVc = self.storyboard?.instantiateViewController(withIdentifier: "MyReportsViewController") as! MyReportsViewController
                
                 let reportDatesVc = self.storyboard?.instantiateViewController(withIdentifier: "ReportsDateViewController") as! ReportsDateViewController
                reportDatesVc.viewModel.categoryId =  self.viewModel.reportDetailsModel?.categoryId ?? 0
                        self.navigationController?.viewControllers = [reportVc ,reportDatesVc]
                
                        NotificationCenter.default.post(name: Notification.Name("leftMenuReload"), object: ["index":1])
            }
            
        }
        
    }
    
    
}
extension ReportDetailViewController : UITableViewDataSource , UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch   viewModel.reportDetailsModel?.questions?.indices.contains(viewModel.currentIndex)  {
        case true:
            
            return  section == 2 ?   (viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].options?.count ?? 0 ) : 1
            
        default:
            
            return    0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader", for: indexPath) as! QuestionsTableViewCell
            
            
            cell.lblQuestionNumber.text = "Question \(viewModel.currentIndex + 1)"
            
            cell.lblNumberOfQuestions.text = "Number of Questions \(viewModel.reportDetailsModel?.totalQuestions ?? 0)"
            
            cell.lblCategory.text = viewModel.reportDetailsModel?.categoryName ?? ""
            
            let date = Date.init(milliseconds:viewModel.reportDetailsModel?.time ?? 0)
            
            cell.lblDate.text = date.convertToFormat("dd MMM yyyy hh:mm a")
            
            
            cell.backArrayView.forEach { (view) in
                
                view.isHidden = viewModel.currentIndex == 0
                
            }
            
            cell.nextArrayView.forEach { (view) in
                
                view.isHidden = viewModel.currentIndex ==  (viewModel.reportDetailsModel?.questions?.count ?? 0) - 1
                
            }
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionsTableViewCell", for: indexPath) as! QuestionsTableViewCell
            
            cell.lblQuestions.font = UIFont(name: "Raleway-Medium", size: 17) ?? UIFont()
            cell.lblQuestions.text =  viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].question ?? ""
            cell.bgView.backgroundColor  = UIColor.white
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionsTableViewCell", for: indexPath) as! QuestionsTableViewCell
            
         
            let index =  viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].options?[indexPath.row].optionIndex ?? ""
            
        
            let answerId = viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].options?[indexPath.row].answerId ?? 0
            
            let descriptionStr =  index + (viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].options?[indexPath.row].options ?? "")
            
            let optionStr = "\(index)) " + (viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].options?[indexPath.row].options ?? "")
             
             
            let option = answerId == -1 ? descriptionStr : optionStr
       
            
            let myMutableString = NSMutableAttributedString(string: option as String, attributes: [NSAttributedString.Key.font: UIFont(name: "Raleway", size: 17)!])

            if let nsRange = option.range(of: "Description:- ")?.nsRange(in: option) {
                
                 myMutableString.setAttributes([NSAttributedString.Key.font: UIFont(name: "Raleway-Bold", size: 17)!], range: nsRange)
                
                cell.lblQuestions.attributedText = myMutableString
                
            }
            else {
                      cell.lblQuestions.attributedText = NSMutableAttributedString(string: (option) as String, attributes: [NSAttributedString.Key.font: UIFont(name: "Raleway", size: 17)!] )
            }

            cell.bgView.backgroundColor = answerId == -1 ? UIColor.init(red: 135/255, green: 206/255, blue: 235/255, alpha: 1.0) :     (viewModel.reportDetailsModel?.questions?[viewModel.currentIndex].myanswer == answerId  ?   appColor : UIColor.white)
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return    UITableView.automaticDimension
    }
    
}

extension  ReportDetailViewController : MFMailComposeViewControllerDelegate {
 
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        self.dismiss(animated: true, completion: nil)
        print("sent!")
    }
}
