//
//  LeftMenuViewController.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {
    
    //MARK:- Variables
    
    let nameArray = ["Home", "My Reports","Notifications","Profile", "Settings","Log Out"]
    
    var selectedIndex = 0
    
  lazy  var viewModel = LoginSignUpViewModel()
    //MARK:- Oulets
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.reloadTable),
        name: NSNotification.Name(rawValue: "leftMenuReload"),
        object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc private func reloadTable(notification: NSNotification){
      
        selectedIndex =   JSON( notification.object ?? "")["index"].intValue
        self.tableView.reloadData()
    }
    
    //MARK:- Logout Action
    
    func logoutAction(){
        
        let alert = UIAlertController(title: appName, message: "Do you want to logout?", preferredStyle: .alert)
        for action in ["Logout", "Cancel"]{
            alert.addAction(UIAlertAction(title: action, style: .default, handler: logoutResponseAction))
        }
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    func logoutResponseAction(action: UIAlertAction) {
        if action.title == "Logout" {
            
            viewModel.logout {
                HelperClass.sharedInstance.gotoLogin()
            }
        }
    }
    
}

extension LeftMenuViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell", for: indexPath) as! LeftMenuTableViewCell
        
        
        cell.lblTitle.text = nameArray[indexPath.row]
        
        cell.bgView.backgroundColor  = indexPath.row == selectedIndex ? appColor : UIColor.white
        
        cell.lblTitle.textColor = indexPath.row == selectedIndex ? UIColor.white : UIColor.black
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let navController = sideMenuController?.rootViewController as? UINavigationController else {
            
            return
        }
        self.selectedIndex =  indexPath.row
        self.tableView.reloadData()
        switch nameArray[indexPath.row] {
            
        case "Home":
            
            let vc = AppStoryboard.Home.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
           navController.viewControllers = [vc]
            
        case "My Reports" :
            
            let vc = AppStoryboard.Home.instantiateViewController(withIdentifier: "MyReportsViewController") as! MyReportsViewController
            
          navController.viewControllers = [vc]
            
            
        case "Settings":
            
            let vc = AppStoryboard.Settings.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            
            navController.viewControllers = [vc]
            
        case "Notifications":
            
            let vc = AppStoryboard.Settings.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            
           navController.viewControllers = [vc]
            
            
        case "Profile" :
            
            let vc = AppStoryboard.Profile.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            
            navController.viewControllers = [vc]
            
        case "Log Out" :
            
         logoutAction()
            
        default:
            print(indexPath.row)
        }
        sideMenuController?.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
