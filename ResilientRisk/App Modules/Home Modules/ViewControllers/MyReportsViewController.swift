//
//  MyReportsViewController.swift
//  ResilientRisk
//
//  Created by apple on 14/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
class MyReportsViewController: UIViewController {
    
    //MARK:-Variables
    
    lazy var viewModel  = MyReportViewModel()
    
   
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.getMyReports()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        guard let sideMenu = self.parent?.parent as? LGSideViewController else {
            return
        }
        sideMenu.showLeftView(animated: true, completionHandler: nil)
    }
    
    //MARK:- Get My Reports
    
    func getMyReports(){
        
        viewModel.getMyReports {
            
            DispatchQueue.main.async {
                self.tableView.emptyDataSetSource = self
                
                self.tableView.emptyDataSetDelegate = self
                
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    
}
extension MyReportsViewController : UITableViewDataSource , UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  viewModel.myReportsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyReportsTableViewCell", for: indexPath) as! MyReportsTableViewCell
        
        cell.myReportModel = viewModel.myReportsArray[indexPath.row]
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportsDateViewController") as! ReportsDateViewController
        vc.viewModel.categoryId = viewModel.myReportsArray[indexPath.row].categoryId ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: DZNEmptyStateDataSet
extension MyReportsViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let font = UIFont.init(name: "Raleway", size: 17)
        
        
        let attrs = [NSAttributedString.Key.font:  font as Any,NSAttributedString.Key.foregroundColor: UIColor.black] as [NSAttributedString.Key : Any]
        
        return NSAttributedString(string: "No reports found", attributes: attrs)
        
    }
    

}
