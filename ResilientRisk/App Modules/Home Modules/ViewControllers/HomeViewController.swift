//
//  HomeViewController.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK:-Variables
    
    lazy var  viewModel = HomeDataViewModel()
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        homeApiData()
    }
    
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
        
        guard let sideMenu = self.parent?.parent as? LGSideViewController else {
            return
        }
        sideMenu.showLeftView(animated: true, completionHandler: nil)
        
    }
    
    @IBAction func btnNotificationsAction(_ sender: UIButton) {
        
        let vc = AppStoryboard.Settings.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController

        vc.isFromHome = true

        self.navigationController?.pushViewController(vc , animated: true)
       
    }
    
    
    //MARK:- Home Api Data
    func homeApiData(){
        viewModel.homeData {
            
            self.tableView.reloadData()
        }
        
    }
    
}
extension HomeViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  section == 0 ? 1 :  viewModel.homeDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader", for: indexPath) as! HomeTableViewCell
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
            
            cell.homeData = viewModel.homeDataArray[indexPath.row]
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard indexPath.section != 0  else {
            
            return
        }
    
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategoryInformationViewController") as! CategoryInformationViewController
        vc.viewModel.homeData =  viewModel.homeDataArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
              
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}



