//
//  ReportsDateViewController.swift
//  ResilientRisk
//
//  Created by apple on 19/02/20.
//  Copyright © 2020 apple. All rights reserved.
//


import UIKit

class ReportsDateViewController: UIViewController {
    
   //MARK:-Variables
       
    lazy var viewModel  = MyReportViewModel()
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
       
        getReportsDates()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getReportsDates(){
        
        viewModel.getReportsDates {
            DispatchQueue.main.async {
                self.title = self.viewModel.myReport?.categoryName ?? ""
                self.tableView.reloadData()
            }
        }
    }
    
}
extension ReportsDateViewController : UITableViewDataSource , UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  viewModel.myReport?.myreports?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyReportsTableViewCell", for: indexPath) as! MyReportsTableViewCell
            
        cell.reportDateModel =  viewModel.myReport?.myreports?[indexPath.row]
            return cell
       
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportDetailViewController") as! ReportDetailViewController
        vc.viewModel.isFromMyReport = true 
        vc.viewModel.reportId = viewModel.myReport?.myreports?[indexPath.row].internalIdentifier ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

