//
//  LGSideViewController.swift

//
//  Created by apple on 07/08/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import LGSideMenuController

class LGSideViewController: LGSideMenuController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let homeVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let leftVc = self.storyboard?.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        
        
        let navController = AppStoryboard.Login.instantiateViewController(withIdentifier: "NavParentViewController") as! UINavigationController
        
        navController.setViewControllers( [homeVc], animated: false)
        
        self.leftViewWidth = UIScreen.main.bounds.width / 1.4
        self.rootViewController = navController
        self.leftViewController = leftVc
        self.leftViewStatusBarStyle = .default
        
        self.isLeftViewSwipeGestureEnabled = false
        
        self.leftViewPresentationStyle = .slideBelow
        self.rootViewCoverColorForLeftView = UIColor.black
        self.rootViewCoverAlphaForLeftView = 0.4
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
