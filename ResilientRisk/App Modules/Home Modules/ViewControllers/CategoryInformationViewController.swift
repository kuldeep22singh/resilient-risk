//
//  CategoryInformationViewController.swift
//  ResilientRisk
//
//  Created by apple on 18/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CategoryInformationViewController: UIViewController {

    //MARK:-Variables
       
     lazy var  viewModel = HomeDataViewModel()
    
    //MARK:- Outlets
    @IBOutlet weak var btnBottomAction: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.homeData?.name ?? ""
        
        
        let localdict : [String : Any] =  JSON(standard.value(forKey: "User\(UserProfile.shared.profile.internalIdentifier ?? 0)Category\(viewModel.homeData?.internalIdentifier ?? 0)") ?? "").dictionaryObject ?? [String : Any]()
        
        
        let isLocalResume =  JSON(localdict)["category_id"].int == viewModel.homeData?.internalIdentifier ?? 0
        btnBottomAction.setTitle(isLocalResume == true ? "Continue Assessment" :"Let's Begin", for: .normal)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLetsBeginAction(_ sender: UIButton) {
        
        guard (viewModel.homeData?.totalQuestions ?? 0) > 0 else {
            
            rootVC?.showSwiftyAlert("", "No Questions in this category", false)
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuestionsViewController") as! QuestionsViewController
        vc.viewModel.categoryId = viewModel.homeData?.internalIdentifier ?? 0
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension CategoryInformationViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellFirst", for: indexPath) as! CategoryInfoTableViewCell
                       
            cell.homeData = self.viewModel.homeData
                       return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryInfoTableViewCell", for: indexPath) as! CategoryInfoTableViewCell
                       
                      cell.homeData = self.viewModel.homeData
                       return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

