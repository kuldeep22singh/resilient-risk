//
//  PaymentSuccessView.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class PaymentSuccessView: UIView {
    
    //MARK:- Variables
    
    var completion : (()->())?
    
    //MARK:- Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    init(frame: CGRect , strTitle : String , completion : @escaping (()->()) ) {
        super.init(frame: frame)
        self.completion = completion
        self.setUpData(strTitle : strTitle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpData(strTitle : String){
        
        self.view = Bundle.main.loadNibNamed("PaymentSuccessView", owner: self, options: nil)?.first as? UIView
        
        self.view.frame = self.bounds
        self.lblTitle.text =  strTitle
        self.addSubview(self.view)
        
        
    }
    @IBAction func btnCrossAction(_ sender: UIButton) {
        guard let  completion = self.completion else {
            
            
            return
        }
        completion()
        self.removeFromSuperview()
    }
    
    
    @IBAction func btnBgAction(_ sender: UIButton) {
        guard let  completion = self.completion else {
            
            
            return
        }
        completion()
        self.removeFromSuperview()
    }
    
}
