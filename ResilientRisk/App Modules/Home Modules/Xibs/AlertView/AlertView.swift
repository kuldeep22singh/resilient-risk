//
//  AlertView.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class AlertView: UIView {
    
    //MARK:- Variables
    
    var completion : ((Int)->())?
    
    //MARK:- Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    init(frame: CGRect , strTitle : String ,strDescription : String , completion : @escaping ((Int)->()) ) {
        super.init(frame: frame)
        self.completion = completion
        self.setUpData(strTitle : strTitle, strDescription: strDescription)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpData(strTitle : String  ,strDescription : String ){
        
        self.view = Bundle.main.loadNibNamed("AlertView", owner: self, options: nil)?.first as? UIView
        
        self.view.frame = self.bounds
        self.lblTitle.text =  strTitle
        self.lblDescription.text =  strDescription
        self.addSubview(self.view)
        
        
    }
    
    //MARK:- UI Buttons Actions
    @IBAction func btnCrossAction(_ sender: UIButton) {
        
        self.removeFromSuperview()
    }
    
    @IBAction func btnOkAction(_ sender: UIButton) {
        
        guard let  completion = self.completion else {
            
            
            return
        }
        self.removeFromSuperview()
        completion(1)
    }
    
    @IBAction func btnLaterAction(_ sender: UIButton) {
        guard let  completion = self.completion else {
            
            
            return
        }
        self.removeFromSuperview()
        completion(2)
        
    }
    @IBAction func btnBgAction(_ sender: UIButton) {
        
        self.removeFromSuperview()
    }
    
}
