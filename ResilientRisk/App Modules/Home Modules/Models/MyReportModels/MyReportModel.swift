//
//  MyReportModel.swift
//
//  Created by apple on 27/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class MyReportModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kMyReportModelCategoryIdKey: String = "category_id"
  private let kMyReportModelCategoryNameKey: String = "category_name"
  private let kMyReportModelInternalIdentifierKey: String = "id"
  private let kMyReportModelImageKey: String = "image"
  private let kMyReportModelReportTimeKey: String = "report_time"
  private let kMyReportModelMyreportsKey: String = "myreports"

  // MARK: Properties
  public var categoryId: Int?
  public var categoryName: String?
  public var internalIdentifier: Int?
  public var image: String?
  public var reportTime: Int?
  public var myreports: [MyReportMyreports]?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    categoryId = json[kMyReportModelCategoryIdKey].int
    categoryName = json[kMyReportModelCategoryNameKey].string
    internalIdentifier = json[kMyReportModelInternalIdentifierKey].int
    image = json[kMyReportModelImageKey].string
    reportTime = json[kMyReportModelReportTimeKey].int
    if let items = json[kMyReportModelMyreportsKey].array { myreports = items.map { MyReportMyreports(json: $0) } }
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = categoryId { dictionary[kMyReportModelCategoryIdKey] = value }
    if let value = categoryName { dictionary[kMyReportModelCategoryNameKey] = value }
    if let value = internalIdentifier { dictionary[kMyReportModelInternalIdentifierKey] = value }
    if let value = image { dictionary[kMyReportModelImageKey] = value }
    if let value = reportTime { dictionary[kMyReportModelReportTimeKey] = value }
    if let value = myreports { dictionary[kMyReportModelMyreportsKey] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.categoryId = aDecoder.decodeObject(forKey: kMyReportModelCategoryIdKey) as? Int
    self.categoryName = aDecoder.decodeObject(forKey: kMyReportModelCategoryNameKey) as? String
    self.internalIdentifier = aDecoder.decodeObject(forKey: kMyReportModelInternalIdentifierKey) as? Int
    self.image = aDecoder.decodeObject(forKey: kMyReportModelImageKey) as? String
    self.reportTime = aDecoder.decodeObject(forKey: kMyReportModelReportTimeKey) as? Int
    self.myreports = aDecoder.decodeObject(forKey: kMyReportModelMyreportsKey) as? [MyReportMyreports]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(categoryId, forKey: kMyReportModelCategoryIdKey)
    aCoder.encode(categoryName, forKey: kMyReportModelCategoryNameKey)
    aCoder.encode(internalIdentifier, forKey: kMyReportModelInternalIdentifierKey)
    aCoder.encode(image, forKey: kMyReportModelImageKey)
    aCoder.encode(reportTime, forKey: kMyReportModelReportTimeKey)
    aCoder.encode(myreports, forKey: kMyReportModelMyreportsKey)
  }

}
