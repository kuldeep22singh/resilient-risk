//
//  MyReportMyreports.swift
//
//  Created by apple on 27/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class MyReportMyreports: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kMyReportMyreportsCategoryIdKey: String = "category_id"
  private let kMyReportMyreportsCategoryNameKey: String = "category_name"
  private let kMyReportMyreportsInternalIdentifierKey: String = "id"
  private let kMyReportMyreportsImageKey: String = "image"
  private let kMyReportMyreportsTotalQuestionsKey: String = "total_questions"
  private let kMyReportMyreportsReportTimeKey: String = "report_time"

  // MARK: Properties
  public var categoryId: Int?
  public var categoryName: String?
  public var internalIdentifier: Int?
  public var image: String?
  public var totalQuestions: Int?
  public var reportTime: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    categoryId = json[kMyReportMyreportsCategoryIdKey].int
    categoryName = json[kMyReportMyreportsCategoryNameKey].string
    internalIdentifier = json[kMyReportMyreportsInternalIdentifierKey].int
    image = json[kMyReportMyreportsImageKey].string
    totalQuestions = json[kMyReportMyreportsTotalQuestionsKey].int
    reportTime = json[kMyReportMyreportsReportTimeKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = categoryId { dictionary[kMyReportMyreportsCategoryIdKey] = value }
    if let value = categoryName { dictionary[kMyReportMyreportsCategoryNameKey] = value }
    if let value = internalIdentifier { dictionary[kMyReportMyreportsInternalIdentifierKey] = value }
    if let value = image { dictionary[kMyReportMyreportsImageKey] = value }
    if let value = totalQuestions { dictionary[kMyReportMyreportsTotalQuestionsKey] = value }
    if let value = reportTime { dictionary[kMyReportMyreportsReportTimeKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.categoryId = aDecoder.decodeObject(forKey: kMyReportMyreportsCategoryIdKey) as? Int
    self.categoryName = aDecoder.decodeObject(forKey: kMyReportMyreportsCategoryNameKey) as? String
    self.internalIdentifier = aDecoder.decodeObject(forKey: kMyReportMyreportsInternalIdentifierKey) as? Int
    self.image = aDecoder.decodeObject(forKey: kMyReportMyreportsImageKey) as? String
    self.totalQuestions = aDecoder.decodeObject(forKey: kMyReportMyreportsTotalQuestionsKey) as? Int
    self.reportTime = aDecoder.decodeObject(forKey: kMyReportMyreportsReportTimeKey) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(categoryId, forKey: kMyReportMyreportsCategoryIdKey)
    aCoder.encode(categoryName, forKey: kMyReportMyreportsCategoryNameKey)
    aCoder.encode(internalIdentifier, forKey: kMyReportMyreportsInternalIdentifierKey)
    aCoder.encode(image, forKey: kMyReportMyreportsImageKey)
    aCoder.encode(totalQuestions, forKey: kMyReportMyreportsTotalQuestionsKey)
    aCoder.encode(reportTime, forKey: kMyReportMyreportsReportTimeKey)
  }

}
