//
//  ReportDetailQuestions.swift
//
//  Created by apple on 27/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ReportDetailQuestions: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kReportDetailQuestionsOptionsKey: String = "options"
  private let kReportDetailQuestionsIndexesKey: String = "indexes"
  private let kReportDetailQuestionsMyanswerKey: String = "myanswer"
  private let kReportDetailQuestionsCreatedKey: String = "created"
  private let kReportDetailQuestionsInternalIdentifierKey: String = "id"
  private let kReportDetailQuestionsQuestionKey: String = "question"

  // MARK: Properties
  public var options: [ReportDetailOptions]?
  public var indexes: Int?
  public var myanswer: Int?
  public var created: Int?
  public var internalIdentifier: Int?
  public var question: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    if let items = json[kReportDetailQuestionsOptionsKey].array { options = items.map { ReportDetailOptions(json: $0) } }
    indexes = json[kReportDetailQuestionsIndexesKey].int
    myanswer = json[kReportDetailQuestionsMyanswerKey].int
    created = json[kReportDetailQuestionsCreatedKey].int
    internalIdentifier = json[kReportDetailQuestionsInternalIdentifierKey].int
    question = json[kReportDetailQuestionsQuestionKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = options { dictionary[kReportDetailQuestionsOptionsKey] = value.map { $0.dictionaryRepresentation() } }
    if let value = indexes { dictionary[kReportDetailQuestionsIndexesKey] = value }
    if let value = myanswer { dictionary[kReportDetailQuestionsMyanswerKey] = value }
    if let value = created { dictionary[kReportDetailQuestionsCreatedKey] = value }
    if let value = internalIdentifier { dictionary[kReportDetailQuestionsInternalIdentifierKey] = value }
    if let value = question { dictionary[kReportDetailQuestionsQuestionKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.options = aDecoder.decodeObject(forKey: kReportDetailQuestionsOptionsKey) as? [ReportDetailOptions]
    self.indexes = aDecoder.decodeObject(forKey: kReportDetailQuestionsIndexesKey) as? Int
    self.myanswer = aDecoder.decodeObject(forKey: kReportDetailQuestionsMyanswerKey) as? Int
    self.created = aDecoder.decodeObject(forKey: kReportDetailQuestionsCreatedKey) as? Int
    self.internalIdentifier = aDecoder.decodeObject(forKey: kReportDetailQuestionsInternalIdentifierKey) as? Int
    self.question = aDecoder.decodeObject(forKey: kReportDetailQuestionsQuestionKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(options, forKey: kReportDetailQuestionsOptionsKey)
    aCoder.encode(indexes, forKey: kReportDetailQuestionsIndexesKey)
    aCoder.encode(myanswer, forKey: kReportDetailQuestionsMyanswerKey)
    aCoder.encode(created, forKey: kReportDetailQuestionsCreatedKey)
    aCoder.encode(internalIdentifier, forKey: kReportDetailQuestionsInternalIdentifierKey)
    aCoder.encode(question, forKey: kReportDetailQuestionsQuestionKey)
  }

}
