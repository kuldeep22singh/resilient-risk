//
//  ReportDetailModel.swift
//
//  Created by apple on 27/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ReportDetailModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kReportDetailModelCategoryIdKey: String = "category_id"
  private let kReportDetailModelCategoryNameKey: String = "category_name"
  private let kReportDetailModelQuestionsKey: String = "Questions"
  private let kReportDetailModelTimeKey: String = "time"
  private let kReportDetailModelTotalQuestionsKey: String = "total_questions"
  private let kReportDetailModelPdfFileKey: String = "pdf_file"
  // MARK: Properties
  public var categoryId: Int?
  public var categoryName: String?
  public var questions: [ReportDetailQuestions]?
  public var time: Int?
  public var totalQuestions: Int?
  public var pdfFile: String?
  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    categoryId = json[kReportDetailModelCategoryIdKey].int
    categoryName = json[kReportDetailModelCategoryNameKey].string
    if let items = json[kReportDetailModelQuestionsKey].array { questions = items.map { ReportDetailQuestions(json: $0) } }
    time = json[kReportDetailModelTimeKey].int
    totalQuestions = json[kReportDetailModelTotalQuestionsKey].int
     pdfFile = json[kReportDetailModelPdfFileKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = categoryId { dictionary[kReportDetailModelCategoryIdKey] = value }
    if let value = categoryName { dictionary[kReportDetailModelCategoryNameKey] = value }
    if let value = questions { dictionary[kReportDetailModelQuestionsKey] = value.map { $0.dictionaryRepresentation() } }
    if let value = time { dictionary[kReportDetailModelTimeKey] = value }
     if let value = pdfFile { dictionary[kReportDetailModelPdfFileKey] = value }
    if let value = totalQuestions { dictionary[kReportDetailModelTotalQuestionsKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.categoryId = aDecoder.decodeObject(forKey: kReportDetailModelCategoryIdKey) as? Int
    self.categoryName = aDecoder.decodeObject(forKey: kReportDetailModelCategoryNameKey) as? String
    self.questions = aDecoder.decodeObject(forKey: kReportDetailModelQuestionsKey) as? [ReportDetailQuestions]
    self.time = aDecoder.decodeObject(forKey: kReportDetailModelTimeKey) as? Int
      self.pdfFile = aDecoder.decodeObject(forKey: kReportDetailModelPdfFileKey) as? String
    self.totalQuestions = aDecoder.decodeObject(forKey: kReportDetailModelTotalQuestionsKey) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(categoryId, forKey: kReportDetailModelCategoryIdKey)
    aCoder.encode(categoryName, forKey: kReportDetailModelCategoryNameKey)
    aCoder.encode(questions, forKey: kReportDetailModelQuestionsKey)
    aCoder.encode(time, forKey: kReportDetailModelTimeKey)
    aCoder.encode(totalQuestions, forKey: kReportDetailModelTotalQuestionsKey)
     aCoder.encode(pdfFile, forKey: kReportDetailModelPdfFileKey)
  }

}
