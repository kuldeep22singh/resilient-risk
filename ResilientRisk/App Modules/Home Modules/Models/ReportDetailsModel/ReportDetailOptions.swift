//
//  ReportDetailOptions.swift
//
//  Created by apple on 27/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ReportDetailOptions: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kReportDetailOptionsOptionsKey: String = "options"
  private let kReportDetailOptionsDescriptionValueKey: String = "description"
  private let kReportDetailOptionsOptionIndexKey: String = "option_index"
  private let kReportDetailOptionsAnswerIdKey: String = "answer_id"

  // MARK: Properties
  public var options: String?
  public var descriptionValue: String?
  public var optionIndex: String?
  public var answerId: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    options = json[kReportDetailOptionsOptionsKey].string
    descriptionValue = json[kReportDetailOptionsDescriptionValueKey].string
    optionIndex = json[kReportDetailOptionsOptionIndexKey].string
    answerId = json[kReportDetailOptionsAnswerIdKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = options { dictionary[kReportDetailOptionsOptionsKey] = value }
    if let value = descriptionValue { dictionary[kReportDetailOptionsDescriptionValueKey] = value }
    if let value = optionIndex { dictionary[kReportDetailOptionsOptionIndexKey] = value }
    if let value = answerId { dictionary[kReportDetailOptionsAnswerIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.options = aDecoder.decodeObject(forKey: kReportDetailOptionsOptionsKey) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: kReportDetailOptionsDescriptionValueKey) as? String
    self.optionIndex = aDecoder.decodeObject(forKey: kReportDetailOptionsOptionIndexKey) as? String
    self.answerId = aDecoder.decodeObject(forKey: kReportDetailOptionsAnswerIdKey) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(options, forKey: kReportDetailOptionsOptionsKey)
    aCoder.encode(descriptionValue, forKey: kReportDetailOptionsDescriptionValueKey)
    aCoder.encode(optionIndex, forKey: kReportDetailOptionsOptionIndexKey)
    aCoder.encode(answerId, forKey: kReportDetailOptionsAnswerIdKey)
  }

}
