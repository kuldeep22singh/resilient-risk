//
//  QuestionModel.swift
//
//  Created by apple on 26/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct QuestionModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kQuestionModelCategoryIdKey: String = "category_id"
  private let kQuestionModelUserIdKey: String = "user_id"
  private let kQuestionModelCategoryNameKey: String = "category_name"
  private let kQuestionModelQuestionsKey: String = "Questions"

  // MARK: Properties
  public var categoryId: Int?
  public var userId: Int?
  public var categoryName: String?
  public var questions: [QuestionQuestions]?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    categoryId = json[kQuestionModelCategoryIdKey].int
    userId = json[kQuestionModelUserIdKey].int
    categoryName = json[kQuestionModelCategoryNameKey].string
    if let items = json[kQuestionModelQuestionsKey].array { questions = items.map { QuestionQuestions(json: $0) } }
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = categoryId { dictionary[kQuestionModelCategoryIdKey] = value }
    if let value = userId { dictionary[kQuestionModelUserIdKey] = value }
    if let value = categoryName { dictionary[kQuestionModelCategoryNameKey] = value }
    if let value = questions { dictionary[kQuestionModelQuestionsKey] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
