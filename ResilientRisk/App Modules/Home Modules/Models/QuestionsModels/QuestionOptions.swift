//
//  QuestionOptions.swift
//
//  Created by apple on 24/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct QuestionOptions {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kQuestionOptionsOptionsKey: String = "options"
  private let kQuestionOptionsDescriptionValueKey: String = "description"
  private let kQuestionOptionsOptionIndexKey: String = "option_index"
  private let kQuestionOptionsAnswerIdKey: String = "answer_id"

  // MARK: Properties
  public var options: String?
  public var descriptionValue: String?
  public var optionIndex: String?
  public var answerId: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    options = json[kQuestionOptionsOptionsKey].string
    descriptionValue = json[kQuestionOptionsDescriptionValueKey].string
    optionIndex = json[kQuestionOptionsOptionIndexKey].string
    answerId = json[kQuestionOptionsAnswerIdKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = options { dictionary[kQuestionOptionsOptionsKey] = value }
    if let value = descriptionValue { dictionary[kQuestionOptionsDescriptionValueKey] = value }
    if let value = optionIndex { dictionary[kQuestionOptionsOptionIndexKey] = value }
    if let value = answerId { dictionary[kQuestionOptionsAnswerIdKey] = value }
    return dictionary
  }

}
