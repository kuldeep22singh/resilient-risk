//
//  QuestionQuestions.swift
//
//  Created by apple on 26/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct QuestionQuestions {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kQuestionQuestionsOptionsKey: String = "options"
  private let kQuestionQuestionsLocalAnswerIdKey: String = "local_answer_id"
  private let kQuestionQuestionsIsSkipKey: String = "isSkip"
  private let kQuestionQuestionsInternalIdentifierKey: String = "id"
  private let kQuestionQuestionsCreatedKey: String = "created"
  private let kQuestionQuestionsAnswerGivenKey: String = "answer_given"
  private let kQuestionQuestionsAnswerMessageKey: String = "answer_message"
  private let kQuestionQuestionsIndexesKey: String = "indexes"
  private let kQuestionQuestionsAnswerIdKey: String = "answer_id"
  private let kQuestionQuestionsQuestionKey: String = "question"

  // MARK: Properties
  public var options: [QuestionOptions]?
  public var localAnswerId: Int?
  public var isSkip: Int?
  public var internalIdentifier: Int?
  public var created: Int?
  public var answerGiven: Int?
  public var answerMessage: String?
  public var indexes: Int?
  public var answerId: Int?
  public var question: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    if let items = json[kQuestionQuestionsOptionsKey].array { options = items.map { QuestionOptions(json: $0) } }
    localAnswerId = json[kQuestionQuestionsLocalAnswerIdKey].int
    isSkip = json[kQuestionQuestionsIsSkipKey].int
    internalIdentifier = json[kQuestionQuestionsInternalIdentifierKey].int
    created = json[kQuestionQuestionsCreatedKey].int
    answerGiven = json[kQuestionQuestionsAnswerGivenKey].int
    answerMessage = json[kQuestionQuestionsAnswerMessageKey].string
    indexes = json[kQuestionQuestionsIndexesKey].int
    answerId = json[kQuestionQuestionsAnswerIdKey].int
    question = json[kQuestionQuestionsQuestionKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = options { dictionary[kQuestionQuestionsOptionsKey] = value.map { $0.dictionaryRepresentation() } }
    if let value = localAnswerId { dictionary[kQuestionQuestionsLocalAnswerIdKey] = value }
    if let value = isSkip { dictionary[kQuestionQuestionsIsSkipKey] = value }
    if let value = internalIdentifier { dictionary[kQuestionQuestionsInternalIdentifierKey] = value }
    if let value = created { dictionary[kQuestionQuestionsCreatedKey] = value }
    if let value = answerGiven { dictionary[kQuestionQuestionsAnswerGivenKey] = value }
    if let value = answerMessage { dictionary[kQuestionQuestionsAnswerMessageKey] = value }
    if let value = indexes { dictionary[kQuestionQuestionsIndexesKey] = value }
    if let value = answerId { dictionary[kQuestionQuestionsAnswerIdKey] = value }
    if let value = question { dictionary[kQuestionQuestionsQuestionKey] = value }
    return dictionary
  }

}
