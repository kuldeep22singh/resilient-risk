//
//  QuestionsModel.swift
//  ResilientRisk
//
//  Created by apple on 20/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

struct QuestionsModel  {
    var question : String?
    var answers : [Answer?]
    var isSkip  : Bool?
    var selectedIndex : Int?
    init(question : String? , answers :  [Answer?] , isSkip : Bool? ,selectedIndex : Int? ) {
           self.question = question
           self.answers = answers
           self.isSkip = isSkip
           self.selectedIndex = selectedIndex
       }
    
}
struct Answer   {
    var answers : String?
    
    
    init(answers : String? ) {
        self.answers = answers
       
    }
    
}
