//
//  HomeDataModel.swift
//
//  Created by apple on 26/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct HomeDataModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kHomeDataModelLocalResumeStatusKey: String = "local_resume_status"
  private let kHomeDataModelStatusKey: String = "status"
  private let kHomeDataModelNameKey: String = "name"
  private let kHomeDataModelInternalIdentifierKey: String = "id"
  private let kHomeDataModelImageKey: String = "image"
  private let kHomeDataModelCreatedKey: String = "created"
  private let kHomeDataModelDescriptionValueKey: String = "description"
  private let kHomeDataModelResumeStatusKey: String = "resume_status"
  private let kHomeDataModelTotalQuestionsKey: String = "total_questions"

  // MARK: Properties
  public var localResumeStatus: Int?
  public var status: Int?
  public var name: String?
  public var internalIdentifier: Int?
  public var image: String?
  public var created: Int?
  public var descriptionValue: String?
  public var resumeStatus: Int?
  public var totalQuestions: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    localResumeStatus = json[kHomeDataModelLocalResumeStatusKey].int
    status = json[kHomeDataModelStatusKey].int
    name = json[kHomeDataModelNameKey].string
    internalIdentifier = json[kHomeDataModelInternalIdentifierKey].int
    image = json[kHomeDataModelImageKey].string
    created = json[kHomeDataModelCreatedKey].int
    descriptionValue = json[kHomeDataModelDescriptionValueKey].string
    resumeStatus = json[kHomeDataModelResumeStatusKey].int
    totalQuestions = json[kHomeDataModelTotalQuestionsKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = localResumeStatus { dictionary[kHomeDataModelLocalResumeStatusKey] = value }
    if let value = status { dictionary[kHomeDataModelStatusKey] = value }
    if let value = name { dictionary[kHomeDataModelNameKey] = value }
    if let value = internalIdentifier { dictionary[kHomeDataModelInternalIdentifierKey] = value }
    if let value = image { dictionary[kHomeDataModelImageKey] = value }
    if let value = created { dictionary[kHomeDataModelCreatedKey] = value }
    if let value = descriptionValue { dictionary[kHomeDataModelDescriptionValueKey] = value }
    if let value = resumeStatus { dictionary[kHomeDataModelResumeStatusKey] = value }
    if let value = totalQuestions { dictionary[kHomeDataModelTotalQuestionsKey] = value }
    return dictionary
  }

}
