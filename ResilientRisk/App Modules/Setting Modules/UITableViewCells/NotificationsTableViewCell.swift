//
//  NotificationsTableViewCell.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

  //MARK: OUTLETS
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            bgView.addShadows(color: UIColor.lightGray)
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var notification : NotificationsModels? {
        
        didSet {
            
            self.lblMessage.text  = notification?.message ?? ""
            
            let date = Date.init(milliseconds: notification?.created ?? 0)
            
            self.lblDate.text = date.getElapsedInterval()
            
        }
        
    }
    
}
