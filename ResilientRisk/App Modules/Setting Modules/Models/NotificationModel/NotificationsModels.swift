//
//  NotificationsModels.swift
//
//  Created by apple on 24/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class NotificationsModels: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kNotificationsModelsIsreadKey: String = "isread"
  private let kNotificationsModelsMessageKey: String = "message"
  private let kNotificationsModelsInternalIdentifierKey: String = "id"
  private let kNotificationsModelsCreatedKey: String = "created"

  // MARK: Properties
  public var isread: Int?
  public var message: String?
  public var internalIdentifier: Int?
  public var created: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    isread = json[kNotificationsModelsIsreadKey].int
    message = json[kNotificationsModelsMessageKey].string
    internalIdentifier = json[kNotificationsModelsInternalIdentifierKey].int
    created = json[kNotificationsModelsCreatedKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = isread { dictionary[kNotificationsModelsIsreadKey] = value }
    if let value = message { dictionary[kNotificationsModelsMessageKey] = value }
    if let value = internalIdentifier { dictionary[kNotificationsModelsInternalIdentifierKey] = value }
    if let value = created { dictionary[kNotificationsModelsCreatedKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isread = aDecoder.decodeObject(forKey: kNotificationsModelsIsreadKey) as? Int
    self.message = aDecoder.decodeObject(forKey: kNotificationsModelsMessageKey) as? String
    self.internalIdentifier = aDecoder.decodeObject(forKey: kNotificationsModelsInternalIdentifierKey) as? Int
    self.created = aDecoder.decodeObject(forKey: kNotificationsModelsCreatedKey) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isread, forKey: kNotificationsModelsIsreadKey)
    aCoder.encode(message, forKey: kNotificationsModelsMessageKey)
    aCoder.encode(internalIdentifier, forKey: kNotificationsModelsInternalIdentifierKey)
    aCoder.encode(created, forKey: kNotificationsModelsCreatedKey)
  }

}
