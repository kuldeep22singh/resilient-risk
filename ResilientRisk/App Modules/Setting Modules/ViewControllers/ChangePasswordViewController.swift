//
//  ChangePasswordViewController.swift
//  ResilientRisk
//
//  Created by apple on 14/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    //MARK:- Variables
    
    lazy var viewModel =  LoginSignUpViewModel()
    
    //MARK:- Outlets
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    @IBAction func btnChangeAction(_ sender: UIButton) {
       changePassword()
        
    }
    
    //MARK:- Change Password Api
    func changePassword(){
        
        viewModel.changePassword(oldPassword: txtOldPassword.text ?? "", newpassword: txtNewPassword.text ?? "", confirmPassword: txtConfirmPassword.text ?? "") {
            
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        
        
    }
    
}
