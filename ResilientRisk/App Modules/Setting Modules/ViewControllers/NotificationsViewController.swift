//
//  NotificationsViewController.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//


import UIKit
import DZNEmptyDataSet
class NotificationsViewController: UIViewController {
    
    //Variable
    
    var isFromHome = Bool()
    
    var viewModel = SettingViewModels()
    
    
    //MARK:- Outlets
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
          UIApplication.shared.applicationIconBadgeNumber = 0
        self.tableView.tableFooterView  = UIView()
        
        btnBack.setImage(isFromHome == true ? UIImage.init(named: "arrow") :  UIImage.init(named: "home"), for: .normal)
        
        notificationListing()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        guard isFromHome == false else {
            
            self.navigationController?.popViewController(animated: true)
            return
            
        }
        
        guard let sideMenu = self.parent?.parent as? LGSideViewController else {
            return
        }
        
        sideMenu.showLeftView(animated: true, completionHandler: nil)
    }
    
    
    //MARK:- Notifications Listings
    
    func notificationListing(){
       
        viewModel.notificationsLsiting {
        
            DispatchQueue.main.async {
                self.tableView.emptyDataSetSource = self
                self.tableView.emptyDataSetDelegate = self
                self.tableView.reloadData()
            }
        }
    
    }
    
    func deleteNotification( indexPath: IndexPath){
        
        viewModel.deleteNotification(indexPath: indexPath) {
            self.tableView.reloadData()
        }
        
    }
    
}
extension NotificationsViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return viewModel.notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
        
        cell.notification = viewModel.notificationArray[indexPath.section]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
         return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            print("delete")
            self.deleteNotification(indexPath: indexPath)
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return 10
    }
    
    
}
//MARK: DZNEmptyStateDataSet
extension NotificationsViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let font = UIFont.init(name: "Raleway", size: 22)
        
        
        let attrs = [NSAttributedString.Key.font:  font as Any,NSAttributedString.Key.foregroundColor: UIColor.black] as [NSAttributedString.Key : Any]
        
        return NSAttributedString(string: "No notifications, yet", attributes: attrs)
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let font = UIFont.init(name: "Raleway", size: 15)
        
        let attrs = [NSAttributedString.Key.font:  font as Any,NSAttributedString.Key.foregroundColor: UIColor.darkGray] as [NSAttributedString.Key : Any]
        
        return NSAttributedString(string: "You don't have any notifications right now.\nlet's change that.", attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nonotifications")
    }
    
    
    
}
