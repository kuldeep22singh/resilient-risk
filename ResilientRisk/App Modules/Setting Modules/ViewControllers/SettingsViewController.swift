//
//  SettingsViewController.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    //MARK:- Variables
    
    let nameArray = ["Notifications", "Update Password","Privacy Policy","Terms & Conditions"]
    
    lazy var viewModel = SettingViewModels()
    
    //MARK:- Oulets
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        guard let sideMenu = self.parent?.parent as? LGSideViewController else {
            return
        }
        sideMenu.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func notificationSwitchAction(_ sender: UISwitch) {
        
        let points = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: points) else {
            
            return
        }
        
        print(indexPath)
        
        
        print(sender.state)
        
        notificationOnOffApi(notification_status:  sender.isOn == true ? 1 : 2)
        
        
    }
    
    //MARK:- Notification On Off Api
    
    func notificationOnOffApi(notification_status : Int){
        
        viewModel.notificationOnOff(notification_status: notification_status) {
            
            self.tableView.reloadData()
            
        }
        
    }
    
    
    
}
extension SettingsViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        
        
        cell.lblTitle.text = nameArray[indexPath.row]
        
        cell.notifificationSwitch.isHidden = nameArray[indexPath.row] != "Notifications"
        
        cell.notifificationSwitch.isOn = UserProfile.shared.profile.isNotificationOn == 1
        
        cell.imgArrow.isHidden = nameArray[indexPath.row] == "Notifications"
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch nameArray[indexPath.row] {
            
        case "Update Password" :
            
            
            let vc = AppStoryboard.Settings.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        case  "Privacy Policy" , "Terms & Conditions":
            
            let vc = AppStoryboard.Login.instantiateViewController(withIdentifier: "WebDataViewController") as! WebDataViewController
            
            vc.strTitle = nameArray[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            print(indexPath.row)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
