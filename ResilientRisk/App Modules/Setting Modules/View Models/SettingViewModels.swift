//
//  SettingViewModels.swift
//  ResilientRisk
//
//  Created by apple on 24/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SettingViewModels: NSObject {
    
    var notificationArray = [NotificationsModels]()
    
    
    func notificationsLsiting(onSuccess:@escaping (successResponse))
    {
      
        ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.getNotificationList.rawValue, parameters: ["":""], isShowLoader: true, success: { (json) in
             
                print(json)
                 
            if let array = json["body"].array {
                
                self.notificationArray = array.map({NotificationsModels.init(json: $0)})
                
            }
             self.notificationArray = self.notificationArray.sorted(by: {($0.created ?? 0) > ($1.created ?? 0)})
                DispatchQueue.main.async {
                onSuccess()
                    
                }
                
            }) { (error) in
                 print(error)
                DispatchQueue.main.async {
                onSuccess()
                    
                }
                rootVC?.showSwiftyAlert("", error, false)
                
            }
        
    }

    
    
    
    
    func notificationOnOff(notification_status : Int,onSuccess:@escaping (successResponse))
       {
               let param : Parameters = ["notification_status":notification_status]
               
               ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.notificationOnOff.rawValue, parameters: param, isShowLoader: true, success: { (json) in
                
                   print(json)
                      UserProfile.shared.profile = UserDataModel.init(json: json["body"])
                   DispatchQueue.main.async {
                    rootVC?.showSwiftyAlert("", json["message"].stringValue, true)
                       onSuccess()
                       
                   }
                   
               }) { (error) in
                    print(error)
                   rootVC?.showSwiftyAlert("", error, false)
                   
               }
           
           
       }

    func deleteNotification(indexPath : IndexPath,onSuccess:@escaping (successResponse))
    {
        let param : Parameters = ["notification_id":notificationArray[indexPath.section].internalIdentifier ?? 0]
            
            ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.deleteNotifications.rawValue, parameters: param, isShowLoader: true, success: { (json) in
             
                print(json)
                self.notificationArray.remove(at: indexPath.section)
                DispatchQueue.main.async {
                 rootVC?.showSwiftyAlert("", json["message"].stringValue, true)
                    onSuccess()
                    
                }
                
            }) { (error) in
                 print(error)
                rootVC?.showSwiftyAlert("", error, false)
                
            }
        
        
    }
    
}
