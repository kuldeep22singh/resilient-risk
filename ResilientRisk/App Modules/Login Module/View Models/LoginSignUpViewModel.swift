//
//  LoginSignUpViewModel.swift
//  ResilientRisk
//
//  Created by apple on 21/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class LoginSignUpViewModel: NSObject
    {
    
    //MARK:- Variables
    
    var webData = String()
    
 
    func login(  email:String,password:String , remember : Bool,onSuccess:@escaping (successResponse))
    {
        if CheckValidations.checkLoginData(email: email, password: password)
        {
            let rememberDict : [String : Any] = ["email" : email , "password" :password]
                
            remember == true ? standard.set(rememberDict, forKey: "Remember") : standard.removeObject(forKey: "Remember")
            
            let deviceToken = standard.value(forKey: "device_token") ?? ""
            
            let param : Parameters = ["email":email,"password":password,"device_type":"2","device_token": deviceToken]
            
            ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.login.rawValue, parameters: param, isShowLoader: true, success: { (json) in
             
                print(json)
                
                UserProfile.shared.profile = UserDataModel.init(json: json["body"])
                
                DispatchQueue.main.async {
                    
                    onSuccess()
                  
                }
                
            }) { (error) in
                 print(error)
                rootVC?.showSwiftyAlert("", error, false)
                
            }
        
        }
    }

    func signUp(name : String ,  email:String,password:String , confirmPassword : String , countryCode : String , phoneNumber : String,termsAndCondition : Bool,onSuccess:@escaping (successResponse))
       {
        if CheckValidations.checkSignUpData(name: name, email: email, password: password, confirmPassword: confirmPassword, countryCode: countryCode, phoneNumber: phoneNumber , termsAndCondition : termsAndCondition)
           {
            
               let deviceToken = standard.value(forKey: "device_token") ?? ""
               
            let param : Parameters = ["name" :  name, "email":email,"password":password,"country_code" : countryCode,"number":phoneNumber,"device_type":"2","device_token": deviceToken]
               
               ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.signup.rawValue, parameters: param, isShowLoader: true, success: { (json) in
                
                   print(json)
                   
                   UserProfile.shared.profile = UserDataModel.init(json: json["body"])
                   
                   DispatchQueue.main.async {
                       
                       onSuccess()
                     
                   }
                   
               }) { (error) in
                    print(error)
                   rootVC?.showSwiftyAlert("", error, false)
                   
               }
           
           }
       }
    
    func forgotPassword(  email:String ,onSuccess:@escaping (successResponse))
    {
        if CheckValidations.checkForgotPasswordData(email: email)
        {
            
            let param : Parameters = ["email":email]
            
            ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.forgotPassword.rawValue, parameters: param, isShowLoader: true, success: { (json) in
                
                print(json)
                
                DispatchQueue.main.async {
                    rootVC?.showSwiftyAlert("", json["message"].stringValue, true)
                    onSuccess()
                    
                }
                
            }) { (error) in
                print(error)
                rootVC?.showSwiftyAlert("", error, false)
                
            }
            
        }
    }
    
    
    
    func getWebData(isTermsCondition : Bool , onSuccess:@escaping (successResponse)){
        
        let url = isTermsCondition ?  Apis.baseUrl.rawValue + Apis.term.rawValue :  Apis.baseUrl.rawValue + Apis.getprivacy.rawValue
         
        
        ApiInterface.sharedInstance.getApiRequest(url: url, isShowLoader: true, success: { (json) in
            print(json)
            
            self.webData = json["body"]["content"].stringValue
            DispatchQueue.main.async {
                
                onSuccess()
              
            }
            
        }) { (error) in
            print(error)
            rootVC?.showSwiftyAlert("", error, false)
        }
        
        
        
        
    }
    
    
    func logout(onSuccess:@escaping (successResponse)){
        
        let url = Apis.baseUrl.rawValue + Apis.logout.rawValue
        
        ApiInterface.sharedInstance.postApiRequest(url: url, parameters: ["":""], isShowLoader: true, success: { (json) in
            print(json)
            
            UserProfile.shared.profile = UserDataModel.init(json: JSON([String : Any]()))
            DispatchQueue.main.async {
                
                onSuccess()
                
            }
            
        }) { (error) in
            print(error)
            UserProfile.shared.profile = UserDataModel.init(json: JSON([String : Any]()))
            DispatchQueue.main.async {
                
                onSuccess()
                
            }
        }
     
    }

    func changePassword(oldPassword : String, newpassword:String , confirmPassword : String,onSuccess:@escaping (successResponse))
       {
           if CheckValidations.changePasswordData(oldPassword: oldPassword, newpassword: newpassword, confirmPassword: confirmPassword)
           {
            
               let param : Parameters = ["old_password":oldPassword,"new_password":newpassword,"confirm_new_password":confirmPassword]
               
               ApiInterface.sharedInstance.postApiRequest(url: Apis.baseUrl.rawValue + Apis.changepassword.rawValue, parameters: param, isShowLoader: true, success: { (json) in
                
                   print(json)
                   
                   DispatchQueue.main.async {
                    rootVC?.showSwiftyAlert("", json["message"].stringValue, true)
                       onSuccess()
                       
                   }
                   
               }) { (error) in
                    print(error)
                   rootVC?.showSwiftyAlert("", error, false)
                   
               }
           
           }
       }
}
