//
//  SignUpViewController.swift
//  ResilientRisk
//
//  Created by apple on 12/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import CountryList
class SignUpViewController: UIViewController {
    
    //MARK:- Variables
    
    lazy var viewModel = LoginSignUpViewModel()
    var countryList = CountryList()
    
    //MARK:- Outlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var btnConfirmPasswordEye: UIButton!
    @IBOutlet weak var btnPasswordEye: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var btnCode: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryList.delegate = self
        btnTermsAndCondition.setAttributedTitle(self.setAttributeString(), for: .normal)
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCountryCodeAction(_ sender: UIButton) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnTermsConditionAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebDataViewController") as! WebDataViewController
        
        vc.strTitle = "Terms & Conditions"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCheckBoxAction(_ sender: UIButton) {
        
        btnCheckBox.isSelected = !btnCheckBox.isSelected
    }
    
    @IBAction func signUpAction(_ sender: UIButton) {
        
       signupApi()
    }
    
    
    //MARK:- SetAttributeString
    func setAttributeString() -> NSMutableAttributedString{
        
        let main_string = "I accept the Terms & Conditions."
        
        
        let string_to_color = "Terms & Conditions"
        
        let range = (main_string as NSString).range(of: string_to_color)
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        
        let colorAttribute = [ NSAttributedString.Key.font : UIFont(name: "Raleway", size: 15)! ,
                               
                               NSAttributedString.Key.foregroundColor: appColor]
        
        attribute.addAttributes([ NSAttributedString.Key.font : UIFont(name: "Raleway", size: 15)! ,
                                  
                                  NSAttributedString.Key.foregroundColor: UIColor.black], range: NSRange.init(location: 0, length: main_string.count))
        
        attribute.addAttributes(colorAttribute, range: range)
        
        
        return attribute
    }
    
    
    
    
    @IBAction func btnPasswordEyeAction(_ sender: UIButton) {
        
        btnPasswordEye.isSelected = !btnPasswordEye.isSelected
        
        txtPassword.isSecureTextEntry =  !btnPasswordEye.isSelected
        
    }
    @IBAction func btnConfirmPasswordEyeAction(_ sender: UIButton) {
        btnConfirmPasswordEye.isSelected = !btnConfirmPasswordEye.isSelected
        txtConfirmPassword.isSecureTextEntry =  !btnConfirmPasswordEye.isSelected
    }
    
    @IBAction func btnSignInAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- SignUp Api
    
    func signupApi(){
        
        viewModel.signUp(name:  txtName.text ?? "", email: txtEmail.text ?? "", password: txtPassword.text ?? "", confirmPassword: txtConfirmPassword.text ?? "", countryCode: btnCode.titleLabel?.text ?? "", phoneNumber: txtPhoneNumber.text ?? "" ,termsAndCondition : btnCheckBox.isSelected ) {
            HelperClass.sharedInstance.gotoHome()
        }
        
        
    }
    
    
    
}
extension SignUpViewController : CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.countryCode)
        print(country.phoneExtension)
        print(country.flag ?? "")
        
        btnCode.setTitle( "+\(country.phoneExtension)", for: .normal)
    }
}
