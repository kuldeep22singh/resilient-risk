//
//  LoginViewController.swift
//  ResilientRisk
//
//  Created by apple on 11/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    //MARK:- Variables
    
  lazy var viewModel = LoginSignUpViewModel()
    
    //MARK:- Outlets
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.text = JSON(standard.value(forKey: "Remember") ?? [String : Any]())["email"].stringValue
              
         txtPassword.text = JSON(standard.value(forKey: "Remember")  ?? [String : Any]())["password"].stringValue
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnRememberAction(_ sender: UIButton) {
        
        btnCheckBox.isSelected = !btnCheckBox.isSelected
    }
    @IBAction func btnEyeAction(_ sender: UIButton) {
        
        txtPassword.isSecureTextEntry =   btnEye.isSelected
        
        btnEye.isSelected = !btnEye.isSelected
        
    }
    
    @IBAction func btnSignUpAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignInAction(_ sender: UIButton) {
        
        standard.removeObject(forKey: "showSubscription")
        loginApi()
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Login Api
    
    func loginApi(){
        
        viewModel.login(email: self.txtEmail.text ?? "", password: txtPassword.text ?? "", remember : btnCheckBox.isSelected, onSuccess: {
            
              HelperClass.sharedInstance.gotoHome()
        })
        
    }
    
}
