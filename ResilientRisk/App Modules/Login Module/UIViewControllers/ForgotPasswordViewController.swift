//
//  ForgotPasswordViewController.swift
//  ResilientRisk
//
//  Created by apple on 12/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

     //MARK:- Variables
    
    lazy var viewModel = LoginSignUpViewModel()
    
    //MARK:- Outlets
    @IBOutlet weak var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        self.forgotPasswordApi()
    }
    
  func forgotPasswordApi(){
        
    viewModel.forgotPassword(email: txtEmail.text ?? "") {
        
           self.navigationController?.popViewController(animated: true)
    }
        
    }
    
}
