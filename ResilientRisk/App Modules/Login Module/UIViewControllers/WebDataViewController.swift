//
//  WebDataViewController.swift
//  ResilientRisk
//
//  Created by apple on 12/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class WebDataViewController: UIViewController {

    //MARK:- Variable
    
    lazy var viewModel = LoginSignUpViewModel()
    
    var strTitle = String()
    
    //MARK:- Outlets
    @IBOutlet weak var txtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = strTitle
          loadWebData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- Load Web Data
    func loadWebData(){
        
        viewModel.getWebData(isTermsCondition: strTitle == "Terms & Conditions") {
            
              DispatchQueue.main.async {
                self.txtView.attributedText = self.viewModel.webData.htmlToAttributedString
               }
        }
        
    }
    
}
