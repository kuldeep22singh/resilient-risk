//
//  UserProfile.swift
//  ResilientRisk
//
//  Created by apple on 21/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

class UserProfile: NSObject {
    static let shared = UserProfile()
    
    var profile : UserDataModel {
        get {
            let dict = standard.dictionary(forKey: "userData")
            return UserDataModel.init(object: dict ?? [:])
        }
        set {
            let dict = newValue.dictionaryRepresentation()
            standard.set(dict, forKey: "userData")
        }
    }
    
    var token : String {
        get {
            return profile.authorizationKey ?? ""
        }
    }
    var userId : Int {
        get {
            return profile.internalIdentifier ?? 0
        }
    }
    
}
