//
//  UserDataModel.swift
//
//  Created by apple on 21/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct UserDataModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kUserDataModelNameKey: String = "name"
  private let kUserDataModelEmailKey: String = "email"
  private let kUserDataModelNumberKey: String = "number"
  private let kUserDataModelAuthorizationKeyKey: String = "authorization_key"
  private let kUserDataModelDeviceTypeKey: String = "device_type"
  private let kUserDataModelStatusKey: String = "status"
  private let kUserDataModelModifiedKey: String = "modified"
  private let kUserDataModelModifiedsKey: String = "modifieds"
  private let kUserDataModelInternalIdentifierKey: String = "id"
  private let kUserDataModelImageKey: String = "image"
  private let kUserDataModelDeviceTokenKey: String = "device_token"
  private let kUserDataModelCreatedKey: String = "created"
  private let kUserDataModelCountryCodeKey: String = "country_code"
  private let kUserDataModelIsNotificationOnKey: String = "isNotificationOn"
  private let kUserDataModelIsPurchasePlanKey: String = "isPurchasePlan"

  // MARK: Properties
  public var name: String?
  public var email: String?
  public var number: String?
  public var authorizationKey: String?
  public var deviceType: Int?
  public var status: Int?
  public var modified: Int?
  public var modifieds: String?
  public var internalIdentifier: Int?
  public var image: String?
  public var deviceToken: String?
  public var created: Int?
  public var countryCode: String?
  public var isNotificationOn: Int?
  public var isPurchasePlan: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    name = json[kUserDataModelNameKey].string
    email = json[kUserDataModelEmailKey].string
    number = json[kUserDataModelNumberKey].string
    authorizationKey = json[kUserDataModelAuthorizationKeyKey].string
    deviceType = json[kUserDataModelDeviceTypeKey].int
    status = json[kUserDataModelStatusKey].int
    modified = json[kUserDataModelModifiedKey].int
    modifieds = json[kUserDataModelModifiedsKey].string
    internalIdentifier = json[kUserDataModelInternalIdentifierKey].int
    image = json[kUserDataModelImageKey].string
    deviceToken = json[kUserDataModelDeviceTokenKey].string
    created = json[kUserDataModelCreatedKey].int
    countryCode = json[kUserDataModelCountryCodeKey].string
    isNotificationOn = json[kUserDataModelIsNotificationOnKey].int
    isPurchasePlan = json[kUserDataModelIsPurchasePlanKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[kUserDataModelNameKey] = value }
    if let value = email { dictionary[kUserDataModelEmailKey] = value }
    if let value = number { dictionary[kUserDataModelNumberKey] = value }
    if let value = authorizationKey { dictionary[kUserDataModelAuthorizationKeyKey] = value }
    if let value = deviceType { dictionary[kUserDataModelDeviceTypeKey] = value }
    if let value = status { dictionary[kUserDataModelStatusKey] = value }
    if let value = modified { dictionary[kUserDataModelModifiedKey] = value }
    if let value = modifieds { dictionary[kUserDataModelModifiedsKey] = value }
    if let value = internalIdentifier { dictionary[kUserDataModelInternalIdentifierKey] = value }
    if let value = image { dictionary[kUserDataModelImageKey] = value }
    if let value = deviceToken { dictionary[kUserDataModelDeviceTokenKey] = value }
    if let value = created { dictionary[kUserDataModelCreatedKey] = value }
    if let value = countryCode { dictionary[kUserDataModelCountryCodeKey] = value }
    if let value = isNotificationOn { dictionary[kUserDataModelIsNotificationOnKey] = value }
    if let value = isPurchasePlan { dictionary[kUserDataModelIsPurchasePlanKey] = value }
    return dictionary
  }

}
