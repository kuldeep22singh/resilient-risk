//
//  ProfileViewModel.swift
//  ResilientRisk
//
//  Created by apple on 24/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ProfileViewModel: NSObject {

    
    func editProfile( image : UIImage?, name : String ,  email:String, countryCode : String , phoneNumber : String,onSuccess:@escaping (successResponse))
    {
     if CheckValidations.editProfileData(name: name, countryCode: countryCode, phoneNumber: phoneNumber)
        {

         let param : Parameters = ["name" :  name, "email":email,"country_code" : countryCode,"number":phoneNumber]

            
            ApiInterface.sharedInstance.multiPartApiRequest(url: Apis.baseUrl.rawValue + Apis.editprofile.rawValue, parameters: param, imagesArray: [image], imageName: ["image"], success: { (json) in
            
                 print(json)
                UserProfile.shared.profile = UserDataModel.init(json: json["body"])
                DispatchQueue.main.async {
                    onSuccess()
                  
                }
            }) { (error) in
                
                 rootVC?.showSwiftyAlert("", error, false)
            }
            


        }



    }


}
