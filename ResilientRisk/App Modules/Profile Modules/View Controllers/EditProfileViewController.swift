//
//  EditProfileViewController.swift
//  ResilientRisk
//
//  Created by apple on 14/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import CountryList
import SKPhotoBrowser
class EditProfileViewController: UIViewController {
    //MARK:- Outlets
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    //MARK:- Variables
    lazy var viewModel = ProfileViewModel()
    var imagePicker: ImagePicker!
    var countryList = CountryList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         countryList.delegate = self
        
        imgUser.layer.borderColor = UIColor.lightText.cgColor
        imgUser.layer.borderWidth = 5
        
        self.imagePicker = ImagePicker(presentationController: self, pickerOption: .image, delegate: self)
        
        setData()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
          guard let sideMenu = self.parent?.parent as? LGSideViewController else {
                  return
              }
              sideMenu.showLeftView(animated: true, completionHandler: nil)
        
    }
    @IBAction func btnFullImageAction(_ sender: UIButton) {
        
        openMedia()
        
    }
    
    func setData(){
        self.txtName.text = UserProfile.shared.profile.name ?? ""
        self.txtEmail.text = UserProfile.shared.profile.email ?? ""
        
        txtPhoneNumber.text = UserProfile.shared.profile.number ?? ""
        
        btnCode.setTitle(UserProfile.shared.profile.countryCode ?? "", for: .normal)
        
        
        var strImage = UserProfile.shared.profile.image ?? ""
            
            strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        let url = URL(string: strImage)
        
        SDWebImageManager.shared.loadImage(with: url, options: .refreshCached, progress: { (a, b, url) in
            
        }) { (image, data, error, type, bool, url) in
            image?.accessibilityHint = "image"
            self.imgUser.image = image ?? UIImage(named:"imgUserPlace")!
            
            
        }
    }
    
    //MARK :- Open Media
    func openMedia(){
        
        var images = [SKPhoto]()
        
        let photo = SKPhoto.photoWithImage(self.imgUser.image ?? UIImage(named:"imgUserPlace")! )
        photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
        images.append(photo)
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
        
        
        self.present(browser, animated: true, completion: {})
    }
    
    @IBAction func btnSelectImageAction(_ sender: UIButton) {
        
           self.imagePicker.present(from: sender)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
    
        
        editProfileApi()
    }
    
    @IBAction func btnCountryCodeAction(_ sender: UIButton) {
           let navController = UINavigationController(rootViewController: countryList)
           self.present(navController, animated: true, completion: nil)
           
       }
    
    //MARK:- Edit Profile Api
    func editProfileApi(){
        
        let image = imgUser.image?.accessibilityHint != nil ? imgUser.image : nil
        
        viewModel.editProfile(image: image, name: self.txtName.text ?? "", email: self.txtEmail.text ?? "", countryCode: btnCode.titleLabel?.text ?? "", phoneNumber: txtPhoneNumber.text ?? "") {
            
                 DispatchQueue.main.async {
                               let vc = PaymentSuccessView.init(frame: rootVC?.view.frame ?? CGRect() , strTitle : "Profile Updated Successfully") {
                
                               }
                
                
                                rootVC?.view.addSubview(vc)
            }
        

            
        }
        
        
        
    }
    
}
extension EditProfileViewController: ImagePickerDelegate {
    
    func didSelectImage(image: UIImage) {
      
    self.imgUser.image = image
        
    }
    
}

//MARK:-  UITextField Delegate
extension EditProfileViewController : UITextFieldDelegate {


func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    //FOR HELP :- https://github.com/orucanil/StringFormatter
    
    guard let text = textField.text else {
        return true
    }
    let lastText = (text as NSString).replacingCharacters(in: range, with: string) as String
    
    if txtPhoneNumber == textField {
         print(text)
        print(string)
        print(lastText)
        textField.text = lastText.format("nnn-nnn-nnnn", oldString: text)
      
        return false
    }
    return true
}
}

extension EditProfileViewController : CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.countryCode)
        print(country.phoneExtension)
        print(country.flag ?? "")
        
        btnCode.setTitle( "+\(country.phoneExtension)", for: .normal)
    }
}
