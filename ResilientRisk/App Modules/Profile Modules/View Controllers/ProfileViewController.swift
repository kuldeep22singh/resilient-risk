//
//  ProfileViewController.swift
//  ResilientRisk
//
//  Created by apple on 14/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var imgUser: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgUser.layer.borderColor = UIColor.lightText.cgColor
        imgUser.layer.borderWidth = 5
        
        // Do any additional setup after loading the view.
        setData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
    }
    
    func setData(){
        self.txtName.text = UserProfile.shared.profile.name ?? ""
        self.txtEmail.text = UserProfile.shared.profile.email ?? ""
        
        txtPhoneNumber.text = "\(UserProfile.shared.profile.countryCode ?? "")\(UserProfile.shared.profile.number ?? "")"
        
        var strImage = UserProfile.shared.profile.image ?? ""
        
        strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        let url = URL(string: strImage)
        
        SDWebImageManager.shared.loadImage(with: url, options: .refreshCached, progress: { (a, b, url) in
            
        }) { (image, data, error, type, bool, url) in
            image?.accessibilityHint = "image"
            self.imgUser.image = image ?? UIImage(named:"imgUserPlace")!
            
            
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        guard let sideMenu = self.parent?.parent as? LGSideViewController else {
            return
        }
        sideMenu.showLeftView(animated: true, completionHandler: nil)
        
    }
    @IBAction func btnEditProfileAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
}
