//
//  NetworkReachability.swift
//  Ticketlink
//
//  Created by apple on 22/11/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import EFInternetIndicator

class NetworkReachability: NSObject {
    
    static let sharedInstance = NetworkReachability()
    
    let reachability = Reachability()!
    
    private override init() {
        super.init()
        self.checkConnection()
    }
    
    func checkConnection() {

     NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
 
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none{
           
           
         self.removeNoConnectionView()
       print("Network reachable")
        
        } else {
            print("Network Not reachable")
                self.addNoConnectionView()
        }
    }

    
    
    

    func addNoConnectionView(){
        
           let window =  UIApplication.shared.windows.first
        
        if window?.viewWithTag(876) as? ReachabilityView == nil{
            let bounds = UIScreen.main.bounds
            let reachableView = ReachabilityView.init(frame: window?.frame ?? bounds)
            reachableView.tag = 876

           window?.addSubview(reachableView)

            reachableView.transform = CGAffineTransform(translationX: 0, y: reachableView.frame.height)
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCurlUp, animations: {
                reachableView.transform = .identity
            }) { (finished) in
            }
        }
    }
    
    
 
    
    func removeNoConnectionView(){
            let window =  UIApplication.shared.windows.first
        guard let reachableView = window?.viewWithTag(876) else {return}
        UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCurlUp, animations: {
            reachableView.transform = CGAffineTransform(translationX: 0, y: reachableView.frame.height)
        }) { (finished) in
            reachableView.removeFromSuperview()
        }
    }

}
