//
//  ReachabilityView.swift
//  Ticketlink
//
//  Created by apple on 22/11/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Lottie

class ReachabilityView: UIView {

    //MARK: OUTLETS
    
    @IBOutlet var bgView: UIView!
    
    @IBOutlet weak var tryAgainBtnOut: UIButton!
    
    @IBOutlet weak var innerView: UIView!
    
    
    //MARK: VARIABLES
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    
    func setup(){
        self.bgView = Bundle.main.loadNibNamed("ReachabilityView", owner: self, options: nil)?.first as? UIView
        self.bgView.frame = self.bounds
        
        self.addSubview(self.bgView)
        //self.addAnimation()
    }
    
    
    func addAnimation(){
        let animation = AnimationView(name: "lottie_thunder")
        animation.frame =  CGRect(x: 0, y: 0, width: self.innerView.frame.width, height: self.innerView.frame.height)
        animation.contentMode = .scaleAspectFill
        animation.loopMode = .loop
        animation.animationSpeed = 1
        animation.play()
        self.innerView.addSubview(animation)
    }
    
    
    //MARK: BUTTONS ACTIONS
    
    @IBAction func tryAgainBtn(_ sender: UIButton) {
        NetworkReachability.sharedInstance.checkConnection()
    }
    

}
