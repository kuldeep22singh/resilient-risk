

//Account Used

// Apple Developer Account
//Id : dharmani.sunaxi@gmail.com
//Password : Aarav25@

// developer@app.resilientriskmanagement.ca


//password is q8F3w0l4uMCl

//Gmail Login

//Gmail: resilientriskmanagement@gmail.com

//Password : 6Ud30K%RIF


let appName = "Resilient Families"

let appColor = UIColor.init(red: 243/255, green: 178/255, blue: 37/255, alpha: 1.0)

let standard = UserDefaults.standard

typealias successResponse = ( ()->())


enum AppStoryboard{
    
    static let Login      = instance("Login")
    static let Home       = instance("Home")
    static let Settings   = instance("Settings")
    static let Profile    = instance("Profile")
    
    static func instance(_ value: String)-> UIStoryboard {
        return UIStoryboard(name: value, bundle: Bundle.main)
    }
}

//RootViewController
var rootVC: UIViewController?{
    get{
        return  UIApplication.shared.windows.first?.rootViewController
    }
    set{
        UIApplication.shared.windows.first?.rootViewController = newValue
    }
}

enum Apis : String {
    
    case baseUrl = "https://resilient.clickandon.com/api/"//   "http://192.168.1.159/resilient/api/" 
    
    case  login = "login"
    
    case signup = "signup"
    
    case getprivacy = "getprivacy"
    
    case term = "term"
    
    case logout = "logout"
    
    case changepassword = "changepassword"
    
    case editprofile = "editprofile"
    
    case notificationOnOff = "notificationOnOff"
    
    case getNotificationList = "getNotificationList"
    
    case deleteNotifications = "deleteNotifications"
    
    case purhcaseSubscription = "purhcaseSubscription"
    
    case gethomedata = "gethomedata"
    
    case getQuestions = "getQuestions"
    
    case restartQuestion = "restartQuestion"
    
    case saveExitQuestions = "saveExitQuestions"
    
    case getMyReports = "getMyReports"
    
    case getReport = "getReport"
    
    case savereport = "savereport"
    
    case forgotPassword = "forgotPassword"
    
    case reportDates = "reportDates"
    
}
