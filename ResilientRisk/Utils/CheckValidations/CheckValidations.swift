//
//  CheckValidations.swift
//  ResilientRisk
//
//  Created by apple on 21/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CheckValidations
{
    class func checkLoginData(email:String,password:String) -> Bool
    {
        if email.isEmpty
        {
            
            rootVC?.showSwiftyAlert("", "Please enter email", false)
            
            
            return false
        }
        else if !email.isValidEmail()
        {
            rootVC?.showSwiftyAlert("", "Please enter valid email", false)
            return false
        }
        else if password.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter password", false)
            return false
        }
        return true
    }
    
    class func checkSignUpData(name : String ,email:String,password:String, confirmPassword : String,countryCode: String, phoneNumber : String , termsAndCondition : Bool) -> Bool
    {
        
        if name.isEmpty
        {
            
            rootVC?.showSwiftyAlert("", "Please enter name", false)
            
            
            return false
        }
        
        if email.isEmpty
        {
            
            rootVC?.showSwiftyAlert("", "Please enter email", false)
            
            
            return false
        }
        else if !email.isValidEmail()
        {
            rootVC?.showSwiftyAlert("", "Please enter valid email", false)
            return false
        }
        else if password.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter password", false)
            return false
        }
            if password.isValidPassword() == false
            {
                rootVC?.showSwiftyAlert("", "Password must be at least 6 characters", false)
                return false
            }
            
        else if confirmPassword.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter confirm password", false)
            return false
        }
        else if password != confirmPassword
        {
            rootVC?.showSwiftyAlert("", "Passwords don't match", false)
            return false
        }
        
        else if !termsAndCondition {
            
            rootVC?.showSwiftyAlert("", "Please accept the Terms & Conditions", false)
            return false
        }
        return true
    }
    
    class func changePasswordData(oldPassword : String, newpassword:String , confirmPassword : String) -> Bool
    {
        if oldPassword.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter old password", false)
            return false
        }
            
            
        else if newpassword.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter new password", false)
            return false
        }
        if newpassword.isValidPassword() == false
        {
            rootVC?.showSwiftyAlert("", "New Password must be at least 6 characters", false)
            return false
        }
            
            
        else if confirmPassword.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter confirm password", false)
            return false
        }
        else if newpassword != confirmPassword
        {
            rootVC?.showSwiftyAlert("", "Passwords don't match", false)
            return false
        }
        return true
    }
    
    class func editProfileData(name : String, countryCode:String , phoneNumber : String) -> Bool
    {
        if name.isEmpty
        {
            rootVC?.showSwiftyAlert("", "Please enter name", false)
            return false
        }
        
        return true
    }
    
    class func checkForgotPasswordData(email:String) -> Bool
    {
        if email.isEmpty
        {
            
            rootVC?.showSwiftyAlert("", "Please enter email", false)
            
            
            return false
        }
        else if !email.isValidEmail()
        {
            rootVC?.showSwiftyAlert("", "Please enter valid email", false)
            return false
        }
        
        return true
    }
}
