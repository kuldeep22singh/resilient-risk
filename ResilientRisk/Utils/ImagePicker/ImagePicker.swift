//
//  ImagePicker.swift
//  FindThat
//
//  Created by apple on 04/09/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

  @objc public protocol ImagePickerDelegate: class {
    @objc optional  func didSelectImage(image: UIImage)
    @objc optional  func didSelectVideo(video: URL)
}


enum pickerOption : Int {
    
    case image
    case video
    case both
  
}

open class ImagePicker: NSObject {
    
    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?
    
    
    
     init(presentationController: UIViewController, pickerOption : pickerOption, delegate: ImagePickerDelegate) {
        self.pickerController = UIImagePickerController()
        
        super.init()
        
        self.presentationController = presentationController
        self.delegate = delegate
        
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        
        if pickerOption == .image {
            self.pickerController.mediaTypes = ["public.image"]
        }
        else  if pickerOption == .video {
            self.pickerController.mediaTypes = [ "public.movie"]
        }
        else {
            self.pickerController.mediaTypes = ["public.image", "public.movie"]
        }
        
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    public func present(from sourceView: UIView) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
//        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
//            alertController.addAction(action)
//        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        
        self.presentationController?.present(alertController, animated: true)
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        
        if let image = image{
            image.accessibilityHint = "image"
            self.delegate?.didSelectImage!(image: image)
        }
        
       
    }
    
    private func pickerControllerVideo(_ controller: UIImagePickerController, didSelect video: URL?) {
        controller.dismiss(animated: true, completion: nil)
        
        if let video = video{
        
            self.delegate?.didSelectVideo!(video: video)
        }
        
        
    }
    
}

extension ImagePicker: UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
          self.pickerControllerVideo(picker, didSelect: nil)
        
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        
        if  let image = info[.editedImage] as? UIImage  {
            return self.pickerController(picker, didSelect: image)
        }
        if  let video = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
          return self.pickerControllerVideo(picker, didSelect: video)
        }
       
        
    
    }
}

