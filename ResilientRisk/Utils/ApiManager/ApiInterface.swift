//
//  ApiInterface.swift
//  GlobeX
//
//  Created by apple on 09/09/19.
//  Copyright © 2019 cqli. All rights reserved.
//

import UIKit

class ApiInterface: NSObject {
    static let sharedInstance = ApiInterface()
    
    func getApiRequest(url : String, isShowLoader : Bool,success:@escaping (JSON) -> Void, failure:@escaping (String) -> Void){
        
        print(url)
        
        if isShowLoader == true {
            rootVC?.showLoader()
        }
        
        ApiManager.requestGETURL(url, header: createHeader(), success: { (json, statusCode) in
            
            if isShowLoader == true {
                rootVC?.hideLoader()
            }
            
            if statusCode == 200{
                success(json)
            } else{
                
                if json["message"].stringValue == "Invalid authorization key" {
                    
                  HelperClass.sharedInstance.gotoLogin()
                    
                    failure("Session Expire")
                }
                else{
                    failure(json["message"].stringValue)
                }
                
                
            }
        }, failure: { (error) in
            if isShowLoader == true {
                rootVC?.hideLoader()
            }
            failure(error.localizedDescription)
        })
        
        
    }
    func postApiRequest(url : String ,parameters : Parameters, isShowLoader : Bool, success:@escaping (JSON) -> Void, failure:@escaping (String) -> Void){
        
        print(url)
        
        if isShowLoader == true {
            rootVC?.showLoader()
        }
        
        ApiManager.requestPOSTURL(url, header: createHeader(), parameters: parameters, success: { (json, statusCode) in
            if isShowLoader == true {
                rootVC?.hideLoader()
            }
            if statusCode == 200{
                success(json)
            } else{
                
                if json["message"].stringValue == "Invalid authorization key" {
                    
                    HelperClass.sharedInstance.gotoLogin()
                    
                    failure("Session Expire")
                }
                else{
                    failure(json["message"].stringValue)
                }
                
                
                
                
            }
        }, failure: { (error) in
            if isShowLoader == true {
                rootVC?.hideLoader()
            }
            failure(error.localizedDescription)
        })
        
    }
    
    func multiPartApiRequest(url : String ,parameters : Parameters, imagesArray : [UIImage?]?, imageName: [String], success:@escaping (JSON) -> Void, failure:@escaping (String) -> Void){
        
        print(parameters)
        
        rootVC?.showLoader()
        ApiManager.requestMultiPartURL(url ,method : .post, params: parameters as [String : AnyObject], headers: createHeader(), imagesArray : imagesArray, imageName: imageName, success: { (json) in
            rootVC?.hideLoader()
            print(json)
            success(json)
            
        }) { (error) in
            rootVC?.hideLoader()
            failure(error)
        }
        
    }
    
    
}
