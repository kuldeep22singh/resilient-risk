//
//  GlobeX.swift
//  PhotoSpot
//
//  Created by Avatar Singh on 2017-07-21.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
func createHeader() -> [String: String] {
    let header  = [ "authorization-Key":UserProfile.shared.token , "Accept" : "application/json" ,"security-Key" : "resilent"]
    
    return header
}


class ApiManager: NSObject {
    

    
    class func requestPOSTURL(_ strURL: String,header : [String : String]?,parameters: Parameters, success:@escaping (JSON,_ statusCode: Int) -> Void, failure:@escaping (Error) -> Void) {
        
    
        Alamofire.request(strURL, method: .post, parameters: parameters, encoding:JSONEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    let statusCode = JSON(response.response?.statusCode ?? 404).intValue
                    success(JSON(data), statusCode)
                }
                break
                
            case .failure(_):
                failure(response.result.error!)
               
                break
                
            }
        }
        
        
        
    }
    class func requestPOSTFORMDATA(_ strURL: String,header : [String : String]?,parameters: Parameters, success:@escaping (JSON,_ statusCode: Int) -> Void, failure:@escaping (Error) -> Void) {
        
        
        Alamofire.request(strURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    let statusCode = JSON(response.response?.statusCode ?? 404).intValue
                    success(JSON(data), statusCode)
                }
                break
                
            case .failure(_):
                failure(response.result.error!)
              
                break
                
            }
        }
    }
    
    
    class func requestDELETEURL(_ strURL: String,header : [String : String]?,parameters: Parameters, success:@escaping (JSON,_ statusCode: Int) -> Void, failure:@escaping (Error) -> Void) {
        
        
        Alamofire.request(strURL, method: .delete , parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    let statusCode = JSON(response.response?.statusCode ?? 404).intValue
                    success(JSON(data), statusCode)
                }
                break
                
            case .failure(_):
                failure(response.result.error!)
             
                break
                
            }
        }
        
        
        
    }
    
    class func requestPUTURL(_ strURL: String,header : [String : String]?,parameters: Parameters, success:@escaping (JSON,_ statusCode: Int) -> Void, failure:@escaping (Error) -> Void) {
        
        
        Alamofire.request(strURL, method: .put , parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    let statusCode = JSON(response.response?.statusCode ?? 404).intValue
                    success(JSON(data), statusCode)
                }
                break
                
            case .failure(_):
                failure(response.result.error!)
              
                break
                
            }
        }
        
        
        
    }
    
    class func requestPUTURLFORM(_ strURL: String,header : [String : String]?,parameters: Parameters, success:@escaping (JSON,_ statusCode: Int) -> Void, failure:@escaping (Error) -> Void) {
        
        
        Alamofire.request(strURL, method: .put , parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    let statusCode = JSON(response.response?.statusCode ?? 404).intValue
                    success(JSON(data), statusCode)
                }
                break
                
            case .failure(_):
                failure(response.result.error!)
                
                break
                
            }
        }
        
        
        
    }
    
    
    class func requestGETURL(_ strURL: String,header : [String : String]?, success:@escaping (JSON,_ statusCode: Int) -> Void, failure:@escaping (Error) -> Void) {
        
        
        Alamofire.request(strURL, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    let statusCode = JSON(response.response?.statusCode ?? 404).intValue
                    success(JSON(data), statusCode)
                }
                break
                
            case .failure(_):
                failure(response.result.error!)
                
                break
                
            }
        }
        
        
        
    }
    
    
    
    
   
    class    func requestMultiPartURL(_ strURL : String,method : HTTPMethod, params : [String : AnyObject]?, headers : [String : String]? , imagesArray : [UIImage?]?, imageName: [String],  success:@escaping (JSON) -> Void, failure:@escaping (String) -> Void){
    
  Alamofire.upload(multipartFormData: { (multipartFormData) in
            if imagesArray?.count != 0 {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM_dd_yy_HH:mm:ss.SS"
                
                for i in 0..<(imagesArray?.count ?? 0){
                    let img =    (imagesArray?[i] ?? UIImage()).jpegData(compressionQuality: 0.3)  
                    if img != nil{
                        let date = formatter.string(from: Date())
                        multipartFormData.append(img!, withName: "\(imageName[i])", fileName: "swift_file\(date).jpeg", mimeType: "image/jpg")
                    }
                }
            }
            
            for (key, value ) in params! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold:UInt64.init(),
           to: strURL,
           method: method,
           headers:headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    UILabel().text = "\((progress.fractionCompleted * 100)) %"
                    print (Float(progress.fractionCompleted * 100))
                    
                  
                })
                
                
                upload.responseJSON { response in
                    
                    guard ((response.result.value) != nil) else{
                    
                        failure(response.result.error?.localizedDescription ?? "" )
                        return
                    }
                    
                    let resJson = JSON(response.result.value!)

                      let apiStatusCode = JSON(response.response?.statusCode ?? 404).intValue
                    guard ((apiStatusCode == 200)) else{
                   
                        failure(resJson["message"].stringValue)
                        return
                    }
                    success(resJson )
                    
                }
                
            case .failure(let encodingError):
                
                failure(encodingError.localizedDescription )
                
            }
            
        })
    }
    
    
    
  
    
    
}
