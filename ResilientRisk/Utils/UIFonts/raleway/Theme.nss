

@bold: Raleway-Bold;
@semibold: Raleway-SemiBold;
@light: Raleway-Light;
@medium: Raleway-Medium;
@regular:Raleway;

bold {
font-name: @bold;
}

semibold {
font-name: @semibold;
}



light {
font-name: @light;
}

regular {
font-name: @regular;
}

medium {
font-name: @medium;
}


