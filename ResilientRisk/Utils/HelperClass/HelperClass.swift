//
//  HelperClass.swift
//  ResilientRisk
//
//  Created by apple on 13/02/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HelperClass: NSObject {
  static let sharedInstance = HelperClass()
    
    //MARK:- Autologin
    func autoLogin(){
    if UserProfile.shared.token != "" {
        self.gotoHome()
        }
        else {
        self.gotoLogin()
        }
    }
    
    //MARK:- Go to Home
     func gotoHome (){
    
        
        let vc =  AppStoryboard.Home.instantiateInitialViewController()
        
        
        if let window = UIApplication.shared.windows.first {
            
            window.rootViewController = vc
            window.makeKeyAndVisible()
        }
        
        
    }
    
    //MARK:- Go to Login
     func gotoLogin (){
     
        let vc =  AppStoryboard.Login.instantiateInitialViewController()
    
        if let window = UIApplication.shared.windows.first {
            
            window.rootViewController = vc
            window.makeKeyAndVisible()
        }
        
        
    }
    
    
    //MARK:- Go to Notifications
    func gotoNotification (){
        
        
        let vc =  AppStoryboard.Home.instantiateInitialViewController() as? LGSideViewController
        
        
        guard let navController = vc?.rootViewController as? UINavigationController else {
            
            return
        }
        let notificationVc = AppStoryboard.Settings.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        
        navController.viewControllers = [notificationVc]
        
        NotificationCenter.default.post(name: Notification.Name("leftMenuReload"), object: ["index":2])
        
        if let window = UIApplication.shared.windows.first {
            
            window.rootViewController = vc
            window.makeKeyAndVisible()
        }
        
        
    }

    
}
