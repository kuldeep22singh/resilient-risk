//
//  UITextField+Custom.swift
//  UMBooks
//
//  Created by apple on 14/08/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

extension UITextField {
    
    var isEmpty: Bool {
        if self.text == nil || self.text == "" || self.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return true
        }
        return false
    }
    func leftview(){
           let vc = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 44))
           
           self.leftView = vc
           self.leftViewMode = .always
       }
       func rightVC(){
           let vc = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 44))
           
           self.rightView = vc
           self.rightViewMode = .always
       }
       func rightViewWithWidth(width :CGFloat){
           let vc = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
           
           self.rightView = vc
           self.rightViewMode = .always
       }
    func leftViewWithWidth(width :CGFloat){
        let vc = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        
        self.leftView = vc
        self.leftViewMode = .always
    }
    
       func leftviewWithImage(image :UIImage){
           let vc = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 34))
           let btnLeft = UIButton(frame: CGRect(x: 8, y: 2, width: 30, height: 30))
           btnLeft.setImage(image, for: .normal)
           btnLeft.isUserInteractionEnabled = false
           vc.addSubview(btnLeft)
           self.leftView = vc
           self.leftViewMode = .always
       }
       
       func leftview(image :UIImage){
           let vc = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 44))
           let btnLeft = UIButton(frame: CGRect(x: 8, y: 15, width: 14, height: 14))
           btnLeft.setImage(image, for: .normal)
           btnLeft.isUserInteractionEnabled = false
           vc.addSubview(btnLeft)
           self.leftView = vc
           self.leftViewMode = .always
       }
       
       func rightview(Img:UIImage ){
           let vc = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 44))
           let btnLeft = UIButton(frame: CGRect(x: 10, y: 15, width: 14, height: 14))
           btnLeft.setImage(Img, for: .normal)
           btnLeft.isUserInteractionEnabled = false
         
           vc.addSubview(btnLeft)
           self.rightView = vc
           self.rightViewMode = .always
       }
    func setRightView(_ image: UIImage?, _ size: CGSize, _ color: UIColor?){
        if image == nil{
            self.rightView = nil
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        var newImage = image
        if color != nil{
            newImage = image!.withRenderingMode(.alwaysTemplate)
        }
        let imageView = UIImageView(image: newImage)
        imageView.tintColor = color
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]-10-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["v0":imageView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]-10-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["v0":imageView]))
        self.rightView = view
        self.rightViewMode = .always
        self.pedding()
    }
    
    func setLeftView(_ image: UIImage?, _ size: CGSize, _ color: UIColor?){
        if image == nil{
            self.leftView = nil
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        var newImage = image
        if color != nil{
            newImage = image!.withRenderingMode(.alwaysTemplate)
        }
        let imageView = UIImageView(image: newImage)
        imageView.tintColor = color
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]-10-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["v0":imageView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]-10-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["v0":imageView]))
        self.leftView = view
        self.leftViewMode = .always
        self.pedding()
    }
    
    private func pedding(){
        if self.leftView == nil && self.rightView != nil{
            self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.bounds.height))
            self.leftViewMode = .always
        }else if self.leftView != nil && self.rightView == nil{
            self.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.bounds.height))
            self.rightViewMode = .always
        }
    }
    
   
    
    func isValidEmail() -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if let text = self.text{
            return emailTest.evaluate(with: text)
        }else{
            return false
        }
    }
    
    func validateCreditCardFormat()-> (type: CardType, valid: Bool) {
        // Get only numbers from the input string
        let input = self.text!
        
        let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }
        
        // check validity
        valid = luhnCheck(number: numberOnly)
        
        // format
        var formatted4 = ""
        for character in numberOnly {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }
        
        formatted += formatted4 // the rest
        
        // return the tuple
        return (type, valid)
    }
    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = number.reversed().map { String($0) }
        
        for (index,tuple) in digitStrings.enumerated() {
            guard let digit = Int(tuple) else { return false }
            
            let odd = index % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
}

enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}
