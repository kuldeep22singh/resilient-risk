//
//  String+Custom.swift
//  Eat4lessUser
//
//  Created by apple on 29/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

extension String
{
    
    func validStartAnEndTime(endTime : String)-> Bool{
        
        let strStartTime = "12-02-2019 \(self)"
        let strEndTime = "12-02-2019 \(endTime)"
        
         let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let startDate = dateFormatter.date(from: strStartTime)
         let endDate = dateFormatter.date(from: strEndTime)
        
        return (startDate ?? Date()) < (endDate ?? Date())
        
        
    }

   
    
    func getDateWithFormat(format : String)->Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = format
        
        
        if dt != nil {
            let strUTC = dateFormatter.string(from: dt!)
            
                let dateUTC = dateFormatter.date(from: strUTC)
            
            return dateUTC
            
        }
        
        return nil
    }
    
    func getDateFromTimeStamp(format : String) -> String {
    
      let date = Date(timeIntervalSince1970: (JSON(self).doubleValue))
 
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: date)
        
        
        
    }
    
    
    func getTimeFromStamp(format : String) -> String {
        
        let date = Date(timeIntervalSince1970: (JSON(self).doubleValue))
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = format
        
        let dateString = dayTimePeriodFormatter.string(from: date)
        return dateString
    }
    
    
    
    func validStartAnEndDate(endDate : String , format : String)-> Bool{
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  format
        
        let startDate = dateFormatter.date(from: self)
        let endDate = dateFormatter.date(from: endDate)
        
        return (startDate ?? Date()) <= (endDate ?? Date())
        
    }
    
    
    
    
    
    func localToUTC (fromFormat: String, toFormat: String) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
      //  dateFormatter.date
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        
        return  (dt != nil ) ? dateFormatter.string(from: dt!) : ""
    }
    
    func convertToDifferentFormat(fromFormat: String, toFormat: String) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
//dateFormatter.date
        let dt = dateFormatter.date(from: self)
     
        dateFormatter.dateFormat = toFormat
        
        return  (dt != nil ) ? dateFormatter.string(from: dt!) : ""
    }
    
    
    func UTCToLocal( fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
      dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
         return  (dt != nil ) ? dateFormatter.string(from: dt!) : ""
    }
    
    func UTCToLocal( format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        
        
        return  dt
    }
    
    
    func convertToDate( format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        
        
        return  dt
    }
    
    
    func convertUTCToLocal(format: String) -> String {
       
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
       
         let date = Date(timeIntervalSince1970: JSON(self).doubleValue)
         dateFormatter.dateFormat = format
       
         let localTime = dateFormatter.string(from: date)
            print(localTime)
            return localTime
        
    }
    
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func isValidMobileNo() -> Bool {
        
        if(self.count < 10) {
            return false
        } else if(self.count > 12) {
            return false
        } else {
            return true
        }
        
    }
    func isValidPassword() -> Bool {
        let passwordRegex = "^.{6,100}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isValidUsername(Input:String) -> Bool {
        let RegEx = "\\A\\w{7,18}\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    func removeHtmlFromString(inPutString: String) -> String{
        
        return inPutString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    
    
    func TimeStampToString(stamp:String)->String {
        let date = Date(timeIntervalSince1970: JSON(stamp).doubleValue)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate:String = dateFormatter.string(from: date)
        return strDate
    }

}
extension Collection where Iterator.Element == [String:Any
] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:AnyObject]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}
