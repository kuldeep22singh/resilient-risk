//
//  Float+Custom.swift
//  FindThat
//
//  Created by apple on 06/08/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.1f", self) 
    }
    
    func withCommas() -> String {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            return numberFormatter.string(from: NSNumber(value:self))!
        }
    
    func getDateFromTimeStamp(format : String) -> String {
        
        let date = Date(timeIntervalSince1970: (self))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: date)
        
        
        
    }
}
