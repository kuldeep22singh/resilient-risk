//
//  UIView+Custom.swift
//  Wedswing
//
//  Created by Rakesh Kumar on 25/06/18.
//  Copyright © 2018 rakesh. All rights reserved.
//

import UIKit

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    func addShadows(color : UIColor){
            self.layer.shadowColor = color.cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 2
            self.layer.masksToBounds = false
        }

    
    func addShadowBottom(color : UIColor){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:1)
        
    }
    func addShadowTop(color : UIColor){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:-1)
        
    }
    
    
    
    func circularShadow(rad: CGFloat , shadowColor: UIColor, bgColor: UIColor){
        
        // add the shadow to the base view
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        
        self.backgroundColor = bgColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 2.0
        self.layoutIfNeeded()
        
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: rad).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
    
    
    
}
