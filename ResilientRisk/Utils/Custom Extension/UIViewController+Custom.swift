//
//  UIViewController+Custom.swift
//  BahamaEats
//
//  Created by apple on 15/03/19.
//  Copyright © 2019 cqli. All rights reserved.
//

import UIKit
import MBProgressHUD
import AVKit
import AVFoundation
import SwiftMessages
extension UIViewController {
    
    
    
    
    func showLoader(){
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = false
          MBProgressHUD.showAdded(to: self.view, animated: true)
            
        }
    }
    
    func hideLoader(){
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = true
            MBProgressHUD.hide(for: self.view, animated: true)
            
        }
    }
    
    func showSwiftyAlert(_ Title :String,_ body: String ,_ isSuccess : Bool){
        DispatchQueue.main.async {
            let warning = MessageView.viewFromNib(layout: .cardView)
            if isSuccess == true{
                warning.configureTheme(.success)
            }else{
                warning.configureTheme(.error)
            }
            warning.configureDropShadow()
            warning.configureContent(title: Title, body: body)
            warning.button?.isHidden = true
            var warningConfig = SwiftMessages.defaultConfig
            warningConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            warningConfig.duration = .seconds(seconds: 2)
            SwiftMessages.show(config: warningConfig, view: warning)
        }
    }
    
    func setupNav(title: String , appColorNav: Bool , leftButtons : [UIBarButtonItem]? , rightButtons: [UIBarButtonItem]? , showShadow : Bool){
        self.navigationController?.isNavigationBarHidden = false
        self.title = title
        
        let fontNormal =  UIFont.systemFont(ofSize: 18)
        
        if appColorNav == true{
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.barStyle = .black
            //            self.navigationController?.navigationBar.barTintColor = appColor
            self.navigationController?.navigationBar.tintColor = UIColor.white
            
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationBar.prefersLargeTitles = true
                self.navigationItem.largeTitleDisplayMode = .never
                
                let fontColor = UIColor.white
                
                
                let fontLarge = UIFont.systemFont(ofSize: 22)
                let AttributesLarge : [NSAttributedString.Key: Any] = [.foregroundColor: fontColor, .font : fontLarge]
                self.navigationController?.navigationBar.largeTitleTextAttributes = AttributesLarge
                
                let AttributesNormal : [NSAttributedString.Key: Any] = [.foregroundColor: fontColor, .font : fontNormal]
                self.navigationController?.navigationBar.titleTextAttributes = AttributesNormal
                
            } else {
                let fontColor = UIColor.white
                
                let AttributesNormal : [NSAttributedString.Key: Any] = [.foregroundColor: fontColor, .font : fontNormal]
                self.navigationController?.navigationBar.titleTextAttributes = AttributesNormal
            }
        } else{
            
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.barStyle = .default
            self.navigationController?.navigationBar.barTintColor = UIColor.white
            self.navigationController?.navigationBar.tintColor = UIColor.white
            
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationBar.prefersLargeTitles = true
                self.navigationItem.largeTitleDisplayMode = .never
                
                let fontColor = UIColor.black
                
                let fontLarge =  UIFont.systemFont(ofSize: 22)
                let AttributesLarge : [NSAttributedString.Key: Any] = [.foregroundColor: fontColor, .font : fontLarge]
                self.navigationController?.navigationBar.largeTitleTextAttributes = AttributesLarge
                
                let AttributesNormal : [NSAttributedString.Key: Any] = [.foregroundColor: fontColor, .font : fontNormal]
                self.navigationController?.navigationBar.titleTextAttributes = AttributesNormal
            } else {
                let fontColor = UIColor.black
                
                let AttributesNormal : [NSAttributedString.Key: Any] = [.foregroundColor: fontColor, .font : fontNormal]
                self.navigationController?.navigationBar.titleTextAttributes = AttributesNormal
            }
        }
        self.navigationItem.hidesBackButton = true
        
        if showShadow == false {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
        }
        
        
        if (leftButtons?.count ?? 0) > 0{
            self.navigationItem.leftBarButtonItems = leftButtons
        }
        
        if (rightButtons?.count ?? 0) > 0{
            self.navigationItem.rightBarButtonItems = rightButtons
        }
    }
    
    func shareAction(content: [Any] , sender: UIView){
        
        
        let activityViewController = UIActivityViewController(
            activityItems: content,
            applicationActivities: nil)
        
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
        }
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    
    func playVideo(videoString : String){
        
        guard let videoURL = URL(string: videoString) else {
            
            print("Url Not Found")
            return
        }
        
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.showsPlaybackControls = true
        playerViewController.player = player
        playerViewController.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
        
        self.present(playerViewController, animated: true) {
            playerViewController.player?.play()
            
        }

        
    }
    
    
    
}

