//
//  Date+Custom.swift
//  Eat4lessUser
//
//  Created by apple on 29/04/19.
//  Copyright © 2019 apple. All rights reserved.
//
import UIKit
extension Date {
    
    func convertToFormat(_ format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = format
        let date = dateFormatter.string(from: self)
        return date
    }
    
    func stripTime() -> Date {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: self)
        let date = Calendar.current.date(from: components)
        return date!
    }
    
    func daysBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
        return components.day ?? 0
    }
    
    func monthsBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.month], from: self, to: toDate)
        return components.month ?? 0
    }
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: self, to: Date())
        
        if interval.year! > 0 {
            return interval.year! == 1 ? "\(interval.year!)" + " " + "year" :
                "\(interval.year!)" + " " + "years"
        }
        
        if interval.month! > 0 {
            return interval.month! == 1 ? "\(interval.month!)" + " " + "month" :
                "\(interval.month!)" + " " + "months"
        }
        
        if interval.day! > 0 {
            return interval.day! == 1 ? "\(interval.day!)" + " " + "day" :
                "\(interval.day!)" + " " + "days"
        }
        
        if interval.hour! > 0 {
            return interval.hour! == 1 ? "\(interval.hour!)" + " " + "hour" :
                "\(interval.hour!)" + " " + "hours"
        }
        
        if interval.minute! > 0 {
            return interval.minute! == 1 ? "\(interval.minute!)" + " " + "min" :
                "\(interval.minute!)" + " " + "mins"
        }
        return "now"
    }
    
    
    func dateString(format: String ) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func timeBetweenDates(toDate: Date) -> String{
        let calendar = NSCalendar.current
        let timeinterval = toDate.timeIntervalSinceNow
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .short
        
        return formatter.string(from: timeinterval)!
    }
    
        var millisecondsSince1970:Int {
            return Int((self.timeIntervalSince1970 ).rounded())
        }
    
        init(milliseconds:Int) {
            self = Date(timeIntervalSince1970: TimeInterval(milliseconds))
        }

}
