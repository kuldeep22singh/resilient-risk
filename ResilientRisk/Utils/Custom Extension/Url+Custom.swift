//
//  Url+Custom.swift
//  GlobeX
//
//  Created by apple on 26/11/19.
//  Copyright © 2019 cqli. All rights reserved.
//

import UIKit

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
