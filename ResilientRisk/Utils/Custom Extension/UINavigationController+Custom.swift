//
//  UINavigationController+Custom.swift
//  GlobeX
//
//  Created by apple on 24/01/20.
//  Copyright © 2020 cqli. All rights reserved.
//

import UIKit

extension UINavigationController: UIGestureRecognizerDelegate {
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.isEnabled = true;
        interactivePopGestureRecognizer?.delegate = self;
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    
        
        return viewControllers.count > 1
    }
    
}
